package geral;

import java.nio.file.Paths;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class hooks 
{
    public static String cenario;
    utils utils = new utils();
    public static Status status;
    public static boolean ultimoScenario = false;
    private static int contadorExtentReports = 0;
    
    @Before
    public void beforeCenario(Scenario scenario)
    {
    	cenario = scenario.getName();
    	status = Status.FAIL;
    	
        if(contadorExtentReports == 0)
        {
           extentReport.criarArquivoHTML(); 
           contadorExtentReports += 1;
        }
        else contadorExtentReports += 1;
         	
        extentReport.test = extentReport.extentReporter.createTest(scenario.getName());
    }

    @After
    public void afterCenario(Scenario scenario)
    {
    	if(status == Status.FAIL)
    	{
    		geral.extentReport.SalvarLogScreenShotFalha("Ocorreu uma falha durante a execu��o.");
    		extentReport.extentReporter.flush();
	        seleniumActions.webDriver.quit();
    	}
    	else
    	{
    		geral.extentReport.SalvarLogScreenShotSucesso("Execu��o realizada com sucesso.");
    		
    		if(ultimoScenario)
			{
    			extentReport.extentReporter.flush();    
			}
    		
    		seleniumActions.webDriver.quit();
    	}
    }
}