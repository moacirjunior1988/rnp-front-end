# language: pt
# encoding: iso-8859-1
Funcionalidade: Alteracao de Categoria

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar alterar uma categoria com informacoes validas com per�odo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria | codigoCategoria |
      | alterar   | alterar         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                       |
      | Registro inclu�do com sucesso. |

  @finalizado
  Cenario: Validar alterar uma categoria com informacoes validas com per�odo de inscricao fechado
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria | codigoCategoria |
      | alterar   | alterar         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                       |
      | Per�odo de cadastramento do objeto finalizado. |

  @finalizado
  Cenario: Validar alterar uma categoria com informacoes ja cadastradas com per�odo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria               | codigoCategoria                |
      | categoria ja cadastrado | codigo categoria ja cadastrado |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                         |
      | A Categoria informada possui cadastro no objeto. |

  @finalizado
  Cenario: Validar campo obrigatorio categoria da alteracao de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E nao informo um ou todos os campos do cadastro de categoria
      | categoria | codigoCategoria |
      |           | alterar         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                          |
      | O campo categoria e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio codigo categoria da alteracao de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E nao informo um ou todos os campos do cadastro de categoria
      | categoria | codigoCategoria |
      | alterar   |                 |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                                 |
      | O campo codigo categoria e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar todos os campo obrigatorio da alteracao de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico no botao editar categoria
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem_1                                         | mensagem_2                                               |
      | O campo categoria e de preenchimento obrigatorio. | O campo codigo categoria e de preenchimento obrigatorio. |
