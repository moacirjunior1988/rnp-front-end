# language: pt
# encoding: iso-8859-1
Funcionalidade: Alteracao de Detalhamento da Categoria

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar alteracao do detalhamento da categoria com informacoes validas com periodo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico na acao editar de um detalhamento da categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de detalhamento de categoria
      | nomeDatalhamento | limiteColecao |
      | alterar          | alterar       |
    E clico no botao alterar da tela de alteracao do detalhamento da categoria
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alteracao do detalhamento da categoria com informacoes validas com periodo de inscricao fechado
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem                                       |
      | Periodo de cadastramento do objeto finalizado. |

  @finalizado
  Cenario: Validar alteracao de datalhamento da categoria com informacoes ja cadastradas com periodo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico na acao editar de um detalhamento da categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de detalhamento de categoria
      | nomeDatalhamento           | limiteColecao              |
      | detalhamento ja cadastrado | detalhamento ja cadastrado |
    E clico no botao alterar da tela de alteracao do detalhamento da categoria
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem                                               |
      | O Detalhamento informado possui cadastro na categoria. |

  @finalizado
  Cenario: Validar campo obrigatorio nome do detalhamento da alteracao de detalhamento da categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico na acao editar de um detalhamento da categoria
    E nao informo um ou todos os campos do cadastro de detalhamento de categoria
      | nomeDatalhamento | limiteColecao |
      |                  | alterar       |
    E clico no botao alterar da tela de alteracao do detalhamento da categoria
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem                                                     |
      | O campo Nome do Detalhamento e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio limite de colecao da alteracao de detalhamento da categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico na acao editar de um detalhamento da categoria
    E nao informo um ou todos os campos do cadastro de detalhamento de categoria
      | nomeDatalhamento | limiteColecao |
      | alterar          |               |
    E clico no botao alterar da tela de alteracao do detalhamento da categoria
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem                                                  |
      | O campo Limite de Colecao e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar todos os campos obrigatorio da alteracao do detalhamento da categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    E clico na acao editar de um detalhamento da categoria
    E clico no botao alterar da tela de alteracao do detalhamento da categoria
    Entao devera apresentar uma mensagem de validacao da tela de detalhemento da categoria
      | mensagem_1                                                   | mensagem_2                                                |
      | O campo Nome do Detalhamento e de preenchimento obrigatorio. | O campo Limite de Colecao e de preenchimento obrigatorio. |
