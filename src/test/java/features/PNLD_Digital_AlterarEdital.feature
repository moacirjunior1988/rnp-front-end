# language: pt
# encoding: iso-8859-1
Funcionalidade: Alteracao de edital

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    E clico no botao fazer login

  @finalizado
  Cenario: Validar alterando no campo titulo do edital com informacao valida
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei alterar um dos campos obrigatorios do edital
      | contador |
      |        1 |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                       | acao    |
      | Registro alterado com sucesso. | alterar |

  @finalizado
  Cenario: Validar alterando no campo ano de referencia com informacao valida
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei alterar um dos campos obrigatorios do edital
      | contador |
      |        2 |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                       | acao    |
      | Registro alterado com sucesso. | alterar |

  @finalizado
  Cenario: Validar alterando no campo tipo de edital com informacao valida
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei alterar um dos campos obrigatorios do edital
      | contador |
      |        3 |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                       | acao    |
      | Registro alterado com sucesso. | alterar |

  @finalizado
  Cenario: Validar alterando no campo numero do edital com informacao valida
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei alterar um dos campos obrigatorios do edital
      | contador |
      |        4 |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                       | acao    |
      | Registro alterado com sucesso. | alterar |

  @finalizado
  Cenario: Validar campos obrigatorio titulo do edital da alteracao de edital
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei apagar a informacao que esta em um campo obrigatorio do edital
      | campo        |
      | tituloEdital |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                 |
      | O campo Titulo do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campos obrigatorio ano de referencia da alteracao de edital
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei apagar a informacao que esta em um campo obrigatorio do edital
      | campo         |
      | anoReferencia |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                  |
      | O campo Ano de Referencia e de preenchimento obrigatorio. |

  @nuncaExecutar
  Cenario: Validar campos obrigatorio tipo do edital da alteracao de edital
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei apagar a informacao que esta em um campo obrigatorio do edital
      | campo      |
      | tipoEdital |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem
      | mensagem                                               |
      | O campo Tipo do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campos obrigatorio numero do edital da alteracao de edital
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei apagar a informacao que esta em um campo obrigatorio do edital
      | campo        |
      | numeroEdital |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                 |
      | O campo Numero do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar todos os campos obrigatorio da alteracao de edital
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei apagar todos os campos obrigatorios da tela de alterar edital
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem_1                                               | mensagem_2                                                | mensagem_3                                              | mensagem_4                                               |
      | O campo Titulo do Edital e de preenchimento obrigatorio. | O campo Ano de Referencia e de preenchimento obrigatorio. | O campo Tipo do Edital  e de preenchimento obrigatorio. | O campo Numero do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar regra de duplicidade ao alterar um edital cadastrado
    Dado que estou na tela de listagem de edital
    E clico no botao alterar de um edital ja cadastrado
    Quando apresentar a tela de alteracao de edital
    E irei informarar dados de um edital ja cadastrado
      | tituloEdital   | anoReferencia | tipoEdital     | numeroEdital |
      | Edital Teste 4 |          2020 | PNLD LITERARIO |            4 |
    E clico no botao alterar edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                                               | acao      |
      | Existe um edital cadastrado com as mesmas informacoes. | duplicado |
