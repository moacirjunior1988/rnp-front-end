# language: pt
# encoding: iso-8859-1
Funcionalidade: Alteracao de objeto

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar alterando no campo nome do objeto com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | nomeObjeto      | nomeCampoAlterar |
      | Objeto alterado | nomeObjeto       |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo codigo do objeto com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | codigoObjeto | nomeCampoAlterar |
      | alterar      | codigoObjeto     |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo habilitar o cadastro de obras com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | habilitarCadastroObras | nomeCampoAlterar       |
      | Nao                    | habilitarCadastroObras |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo data e hora de abertura com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataHoraAbertura | nomeCampoAlterar |
      | alterar          | dataHoraAbertura |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo data e hora de fechamento com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataHoraFechamento | nomeCampoAlterar   |
      | alterar            | dataHoraFechamento |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo habilitar fases com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | habilitarfases | nomeCampoAlterar |
      |            3,4 | habilitarfases   |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo data inical de inscricao entrega com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataInicialInscricaoEntrada | nomeCampoAlterar            |
      | alterar                     | dataInicialInscricaoEntrada |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo data final de inscricao entrega com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataFinalInscricaoEntrada | nomeCampoAlterar          |
      | alterar                   | dataFinalInscricaoEntrada |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo periodo de data inicial da triagem com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataInicialTriagem | nomeCampoAlterar   |
      | alterar            | dataInicialTriagem |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo periodo de data final da triagem com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataFinalTriagem | nomeCampoAlterar |
      | alterar          | dataFinalTriagem |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo ano(s) de atendimento com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | anoAtendimento | nomeCampoAlterar |
      | alterar        | anoAtendimento   |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo exibir na habilitacao obras na situacao com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | situacaoObras | nomeCampoAlterar |
      | Reprovada     | situacaoObras    |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo limite de obras na situacao com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | limiteObras | nomeCampoAlterar |
      |           7 | limiteObras      |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar alterando no campo data de publicacao no dou na situacao com informacao valida
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataPublicacaoDou | nomeCampoAlterar  |
      | alterar           | dataPublicacaoDou |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado
  Cenario: Validar campo obrigatorio nome objeto da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | nomeObjeto | nomeCampoAlterar |
      |            | nomeObjeto       |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                            |
      | O campo nome objeto e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio codigo do objeto da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | codigoObjeto | nomeCampoAlterar |
      |              | codigoObjeto     |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                 |
      | O campo codigo do objeto e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio habilitar o cadastro de obras da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | habilitarCadastroObras | nomeCampoAlterar       |
      | Nao                    | habilitarCadastroObras |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                              |
      | O campo habilitar o cadastro de obras e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data e hora de abertura da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataHoraAbertura | nomeCampoAlterar |
      |                  | dataHoraAbertura |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                        |
      | O campo data e hora de abertura e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data e hora de fechamento da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataHoraFechamento | nomeCampoAlterar   |
      |                    | dataHoraFechamento |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                          |
      | O campo data e hora de fechamento e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio habilitar fases da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | habilitarfases | nomeCampoAlterar |
      |                | habilitarfases   |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                |
      | O campo habilitar fases e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data inicial do inscricao entrega da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataInicialInscricaoEntrada | nomeCampoAlterar            |
      |                             | dataInicialInscricaoEntrada |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                             |
      | O campo periodo de inscricao entrega e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data final do inscricao entrega da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataFinalInscricaoEntrada | nomeCampoAlterar          |
      |                           | dataFinalInscricaoEntrada |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                             |
      | O campo periodo de inscricao entrega e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data inicial da triagem da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataInicialTriagem | nomeCampoAlterar   |
      |                    | dataInicialTriagem |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                   |
      | O campo periodo de triagem e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data final da triagem da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataFinalTriagem | nomeCampoAlterar |
      |                  | dataFinalTriagem |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                   |
      | O campo periodo de triagem e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio ano(s) de atendimento da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | anoAtendimento | nomeCampoAlterar |
      |                | anoAtendimento   |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                      |
      | O campo ano(s) de atendimento e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio exibir na habilitacao obras na situacao da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | situacaoObras | nomeCampoAlterar |
      |               | situacaoObras    |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                                        |
      | O campo exibir na habilitacao obras na situacao e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio limite de obras da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | limiteObras | nomeCampoAlterar |
      |             | limiteObras      |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                |
      | O campo limite de obras e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data de publicacao no dou da alteracao do objeto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado
      | dataPublicacaoDou | nomeCampoAlterar  |
      |                   | dataPublicacaoDou |
    E clico no botao gravar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                          |
      | O campo data de publicacao no dou e de preenchimento obrigatorio. |
