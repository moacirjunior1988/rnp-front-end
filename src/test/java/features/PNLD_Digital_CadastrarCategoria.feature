# language: pt
# encoding: iso-8859-1
Funcionalidade: Cadastro de categoria

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar cadastro de categoria com informacoes validas com periodo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria | codigoCategoria |
      | inserir   | inserir         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar cadastro de categoria com informacoes validas com periodo de inscricao fechado
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria | codigoCategoria |
      | inserir   | inserir         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                       |
      | Periodo de cadastramento do objeto finalizado. |

  @finalizado
  Cenario: Validar cadastro de categoria com informacoes ja cadastradas com periodo de inscricao aberto
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E informarei corretamente todos os campos obrigatorios do cadastro de categoria
      | categoria               | codigoCategoria                |
      | categoria ja cadastrado | codigo categoria ja cadastrado |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                         |
      | A Categoria informada possui cadastro no objeto. |

  @finalizado
  Cenario: Validar campo obrigatorio categoria do cadastro de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E nao informo um ou todos os campos do cadastro de categoria
      | categoria | codigoCategoria |
      |           | inserir         |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem                                          |
      | O campo categoria e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio codigo categoria do cadastro de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E nao informo um ou todos os campos do cadastro de categoria
      | categoria | codigoCategoria |
      | inserir   |                 |
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem_1                                        | mensagem_2                                               |  |
      | O campo categoria e de preenchimento obrigatorio. | O campo codigo categoria e de preenchimento obrigatorio. |  |

  @finalizado
  Cenario: Validar todos os campo obrigatorio do cadastro de categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias
    E clico no botao cadastrar categoria
    E clico no botao cadastrar da tela de cadastro de categoria
    Entao devera apresentar uma mensagem de validacao da tela de categoria
      | mensagem_1                                        | mensagem_2                                               |
      | O campo categoria e de preenchimento obrigatorio. | O campo codigo categoria e de preenchimento obrigatorio. |
