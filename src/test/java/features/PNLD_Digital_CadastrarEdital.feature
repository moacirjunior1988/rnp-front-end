# language: pt
# encoding: iso-8859-1
Funcionalidade: Cadastro de Edital

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    E clico no botao fazer login

  @finalizado
  Cenario: Validar cadastro de edital com informacoes validas
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E informarei corretamente todos os campos obrigatorios do cadastro de edital
      | tituloEdital | anoReferencia | tipoEdital | numeroEdital |
      | inserir      | inserir       | inserir    | inserir      |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                       | acao    |
      | Registro incluido com sucesso. | inserir |

  @finalizado
  Cenario: Validar campos obrigatorio titulo do edital do cadastro de edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E nao informo um ou todos os campos do cadastro de edital
      | tituloEdital | anoReferencia | tipoEdital | numeroEdital |
      |              | inserir       | PNLD CAMPO | inserir      |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                 |
      | O campo Titulo do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campos obrigatorio ano de referencia do cadastro de edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E nao informo um ou todos os campos do cadastro de edital
      | tituloEdital | anoReferencia | tipoEdital     | numeroEdital |
      | inserir      |               | PNLD LITERARIO | inserir      |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                  |
      | O campo Ano de Referencia e de preenchimento obrigatorio. |

  @nuncaExecutar
  Cenario: Validar campos obrigatorio tipo do edital do cadastro de edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E nao informo um ou todos os campos do cadastro de edital
      | tituloEdital | anoReferencia | tipoEdital | numeroEdital |
      | inserir      | inserir       |            | inserir      |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                |
      | O campo Tipo do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campos obrigatorio numero do edital do cadastro de edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E nao informo um ou todos os campos do cadastro de edital
      | tituloEdital | anoReferencia | tipoEdital | numeroEdital |
      | inserir      | inserir       | PNLD EJA   |              |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem                                                 |
      | O campo Numero do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar todos os campos obrigatorio do cadastro de edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital
      | mensagem_1                                               | mensagem_2                                                | mensagem_3                                              | mensagem_4                                               |
      | O campo Titulo do Edital e de preenchimento obrigatorio. | O campo Ano de Referencia e de preenchimento obrigatorio. | O campo Tipo do Edital  e de preenchimento obrigatorio. | O campo Numero do Edital e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar regra de duplicidade de edital ao cadastrar um novo edital
    Dado que estou na tela de listagem de edital
    E clico no botao cadastrar edital
    Quando apresentar a tela de cadastro de edital
    E informarei corretamente todos os campos obrigatorios do cadastro de edital
      | tituloEdital   | anoReferencia | tipoEdital     | numeroEdital |
      | Edital Teste 4 |          2020 | PNLD LITERARIO |            4 |
    E clico no botao cadastrar o edital
    Entao devera apresentar uma mensagem de validacao de cadastrar e alterar edital
      | mensagem                                               | acao      |
      | Existe um edital cadastrado com as mesmas informacoes. | duplicado |
