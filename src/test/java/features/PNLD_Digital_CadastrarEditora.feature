# language: pt
# encoding: iso-8859-1
Funcionalidade: Cadastro de Editora

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar cadastro de editora com informacoes validas
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          |
    E clico no botao cadastrar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar cadastro de editora nao informando nome fantasia
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     |              | inserir        | inserir         | inserir             | inserir          |
    E clico no botao cadastrar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar campo obrigatorio cnpj do cadastro de editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      |      | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                     |
      | O campo cnpj e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio razao social do cadastro de editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir |             | inserir      | inserir        | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                             |
      | O campo razao social e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio cpf responsavel do cadastro de editora
   Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      |                | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                |
      | O campo cpf responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio nome responsavel do cadastro de editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      | inserir        |                 | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                 |
      | O campo nome responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio telefone responsavel do cadastro de editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      | inserir        | inserir         |                     | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                     |
      | O campo telefone responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio email responsavel do cadastro de editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E nao informo um ou todos os campos do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      | inserir        | inserir         | inserir             |                  |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                                  |
      | O campo email responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar cadastro de editora com cnpj invalido
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj     | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | invalido | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                      |
      | O campo cnpj deve ser valido. |

  @finalizado
  Cenario: Validar cadastro de editora com cpf responsavel invalido
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | inserir | inserir     | inserir      | invalido       | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar regra de duplicidade de editora ao cadastrar um novo editora
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj          | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel |
      | ja cadastrado | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                                     |
      | O CNPJ informado possui cadastro no sistema. |

  @finalizado
  Cenario: Validar cadastro de editora com responsavel ja cadastrado no sistema
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel       | emailResponsavel          |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar cadastro de editora com responsavel ja cadastrado no sistema alterando telefone do responsavel
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel | emailResponsavel          |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado | alterar             | responsavel ja cadastrado |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar cadastro de editora com responsavel ja cadastrado no sistema alterando email do responsavel
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel       | emailResponsavel |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado | alterar          |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar cadastro de editora com responsavel ja cadastrado no sistema removendo o telefone do responsavel
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel | emailResponsavel          |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado |                     | responsavel ja cadastrado |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                                                     |
      | O campo telefone responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar cadastro de editora com responsavel ja cadastrado no sistema removendo o email do responsavel
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja viculadas ao objeto
    E clico no botao cadastrar editora
    E informarei corretamente todos os campos obrigatorios do cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel       | emailResponsavel |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado |                  |
    E clico no botao gravar da tela de cadastro de editora
    Entao devera enviar um email para o responsavel e apresentar uma mensagem de validacao
      | mensagem                                                  |
      | O campo email responsavel e de preenchimento obrigatorio. |
