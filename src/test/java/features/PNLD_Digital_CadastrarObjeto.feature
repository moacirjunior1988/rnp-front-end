# language: pt
# encoding: iso-8859-1
Funcionalidade: Cadastro de Objeto

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    E clico no botao fazer login

  @finalizado
  Cenario: Validar cadastro de objeto com informacoes validas
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    E cadastro um edital
      | tituloEdital | anoReferencia | tipoEdital | numeroEdital |
      | inserir      | inserir       | inserir    | inserir      |
    E pesquiso o edital cadastrado
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E informarei corretamente todos os campos obrigatorios do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | inserir    | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao de cadastro e alteracao do objeto

  @finalizado
  Cenario: Validar campo obrigatorio nome objeto do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      |            | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                     |
      | O campo Nome e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio codigo do objeto do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     |              | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                       |
      | O campo Codigo e de preenchimento obrigatorio. |

  @nuncaExecutar
  Cenario: Validar campo obrigatorio habilitar o cadastro de obras do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      |                        | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                              |
      | O campo habilitar o cadastro de obras e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data e hora de abertura do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    |                  | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                        |
      | O campo Data e Hora de Abertura e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data e hora de fechamento do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          |                    |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                        |
      | O campo Data e Hora de Abertura e de preenchimento obrigatorio. |

  @nuncaExecutar
  Cenario: Validar campo obrigatorio habilitar fases do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |                | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo habilitar fases e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data inicial do periodo de inscricao entrega do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 |                                    | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo Data de Entrega e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data final do periodo de inscricao entrega do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            |                                  | inserir                  | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo Data de Entrega e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data inical periodo de triagem do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          |                          | inserir                 | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo Data de Triagem e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data final periodo de triagem do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  |                         | inserir        | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo Data de Triagem e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio ano de atendimento do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 |                | Aprovado      | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                    |
      | O campo Anos de Atendimento e de preenchimento obrigatorio. |

  @nuncaExecutar
  Cenario: Validar campo obrigatorio exibir na habilitacao obras na situacao do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        |               | inserir     | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                                        |
      | O campo exibir na habilitacao obras na situacao e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio limite de obras do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      |             | inserir           |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                |
      | O campo Limite de Obras e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio data de publicacao no dou do cadastro de objeto
    Dado que estou na tela de listagem de edital com editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    E clico no botao inserir objeto
    Quando apresentar a tela de cadastro de objeto
    E nao informo um ou todos os campos do cadastro de objeto
      | nomeObjeto | codigoObjeto | habilitarCadastroObras | dataHoraAbertura | dataHoraFechamento | habilitarfases | dataInicialPeriodoInscricaoEntrada | dataFinalPeriodoInscricaoEntrada | dataInicalPeriodoTriagem | dataFinalPeriodoTriagem | anoAtendimento | situacaoObras | limiteObras | dataPublicacaoDou |
      | objeto     | inserir      | Sim                    | inserir          | inserir            |            1,2 | inserir                            | inserir                          | inserir                  | inserir                 | inserir        | Aprovado      | inserir     |                   |
    E clico no botao salvar
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto
      | mensagem                                                          |
      | O campo Data de Publicacao no DOU e de preenchimento obrigatorio. |
