# language: pt
# encoding: iso-8859-1
Funcionalidade: Home do sistema PNLD Digital

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar

  @finalizado
  Cenario: Acessar menu edital
    Dado que estou na tela home
    E acesso o menu
      | menu   |
      | edital |
    Entao devera apresentar tela de listagem de edital
    
	@finalizado
  Cenario: Acessar menu solicitacao de cadastro editora
    Dado que estou na tela home
    E acesso o menu
      | menu                         |
      | solicitacao cadastro editora |
    Entao devera apresentar tela de listagem das solicitacoes de cadastramento das editoras
