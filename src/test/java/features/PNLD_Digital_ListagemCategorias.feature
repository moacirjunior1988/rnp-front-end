# language: pt
# encoding: iso-8859-1
Funcionalidade: Listagem de categoria

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar listagem das cadategorias cadastradas
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Entao devera apresentar uma lista de categorias ja cadastradas e a exibindo das colunas
      | coluna_1 | coluna_2  | coluna_3                  |
      | N        | Categoria | Detalhamento da categoria |

  @finalizado
  Cenario: Validar acoes dentro da categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    Entao devera apresentar em cada linha de categorias na coluna categoria as acoes
      | acoes_1 | acoes_2      | acoes_3 |
      | Editar  | Detalhamento | Excluir |

  @finalizado
  Cenario: Validar listagem das categorias vazia
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Entao devera apresentar a tela de listagem de categorias vazia
      | mensagem                    |
      | Nenhum registro encontrado. |
