# language: pt
# encoding: iso-8859-1
Funcionalidade: Listagem do Detalhamento da categoria

  @finalizado
  Cenario: Validar listagem das categorias com detalhamento vazia
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    Entao devera apresentar a tela de listagem de categorias com o detalhamento vazia
      | mensagem                    |
      | Nenhum registro encontrado. |

  @finalizado
  Cenario: Validar acoes dentro do detalhamento da categoria
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    Entao devera apresentar em cada linha de categoria e datelhamento ja cadastradas as acoes na coluna detalhamento
      | acoes_1   | acoes_2 | acoes_3 |
      | ordenacao | Editar  | Excluir |

  @finalizado
  Cenario: Validar listagem das categorias com detalhamento cadastrado
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar      |
      | Edital Teste 1 |
    E clico no botao pesquisar edital
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao editar de um objeto ja cadastrado em um edital
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba categoria
    Quando apresentar a tela de listagem de categorias ja cadastradas
    Entao devera apresentar a tela de listagem de categorias o detalhamento de uma categoria cadastrada
      | nomeDatalhamento     | limiteColecao            |
      | nome do detalhemento | Limitacoes de Colecao: 5 |
