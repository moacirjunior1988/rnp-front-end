# language: pt
# encoding: iso-8859-1
Funcionalidade: Logar no sistema PNLD Digital

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital

  @finalizado
  Cenario: Logando com um usuario FNDE valido
    Dado que estou na tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no recaptche
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema

  @finalizado
  Cenario: Logando com um usuario Editora valido
    Dado que estou na tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+29410241235@redspark.io | Asdf!2345678 |
    E clico no recaptche
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema

  @finalizado
  Cenario: Logando com um usuario IPT valido
    Dado que estou na tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+32826462164@redspark.io | Asdf!2345678 |
    E clico no recaptche
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema

  @finalizado
  Cenario: Logando com um usuario consulta e administrador
    Dado que estou na tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+07130854367@redspark.io | Asdf!2345678 |
    E clico no recaptche
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema

  @finalizado
  Cenario: Validando o campo usuario vazio
    Dado que estou na tela de login do sistema pnld digital
    Quando nao informamos o usuario ou a senha
      | usuario | senha        |
      |         | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                           |
      | Digite um CPF ou um e-mail validos |

  @finalizado
  Cenario: Validando o campo senha vazio
    Dado que estou na tela de login do sistema pnld digital
    Quando nao informamos o usuario ou a senha
      | usuario                                  | senha |
      | andre.dellatorre+83464047865@redspark.io |       |
    E clico no botao entrar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                  |
      | Senha deve ser preenchida |

  @finalizado
  Cenario: Validando usuario com informacoes invalidas no pnld digital
    Dado que estou na tela de login do sistema pnld digital
    Quando informamos o usuario invalido e a senha
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                   |
      | Usuario ou senha invalidos |

  @finalizado
  Cenario: Validando usuario nao cadastrado no pnld digital
    Dado que estou na tela de login do sistema pnld digital
    Quando informar um usuario nao cadastrado e a senha
      | usuario                    | senha     |
      | teste_front@mailinator.com | teste@123 |
    E clico no botao entrar
    Entao devera apresentar uma mensagem de validacao
      | mensagem                   |
      | Usuario ou senha invalidos |