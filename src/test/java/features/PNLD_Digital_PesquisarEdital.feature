# language: pt
# encoding: iso-8859-1
Funcionalidade: Pesquisa de edital

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    E clico no botao fazer login

  @finalizado
  Cenario: Validar listagem de editais ja cadastros
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Entao devera apresentar uma lista de editais exibindo as colunas
      | coluna_1 | coluna_2 | coluna_3 | coluna_4 | coluna_5 | coluna_6 |
      | Titulo   | Ano      | Tipo     | Numero   | objeto   | Acoes    |

  @finalizado
  Cenario: Validar pesquisa de edital pelo titulo do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar        |
      | Edital Teste 709 |

  @finalizado
  Cenario: Validar pesquisa de edital pelo ano do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar |
      |      2020 |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar |
      |      2020 |

  @finalizado
  Cenario: Validar pesquisa de edital pela sigla do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar |
      | E21       |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar |
      | E21       |

  @finalizado
  Cenario: Validar pesquisa de edital sem passar filtro
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    E clico no botao pesquisar edital
    Entao devera retornar todos registros

  @naoExecutar
  Cenario: Validar detalhes do edital pesquisando pelo titulo do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar        |
      | Edital Teste 709 |
    E clico no botao visualizar de um edital
    Entao devera apresentar uma tela com as informacoes detalhadas do edital visualizado

  @naoExecutar
  Cenario: Validar detalhes do edital pesquisando pelo sigla do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar |
      | E21       |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar |
      | E21       |
    E clico no botao visualizar de um edital
    Entao devera apresentar uma tela com as informacoes detalhadas do edital visualizado

  @naoExecutar
  Cenario: Validar detalhes do edital pesquisando pelo ano do edital
    Dado que estou na tela de listagem de edital
    E houver editais ja cadastrados
    Quando informar no campo pesquisar
      | pesquisar |
      |      2020 |
    E clico no botao pesquisar edital
    Entao devera retornar os registros especificos pesquisados
      | pesquisar |
      |      2020 |
    E clico no botao visualizar de um edital
    Entao devera apresentar uma tela com as informacoes detalhadas do edital visualizado