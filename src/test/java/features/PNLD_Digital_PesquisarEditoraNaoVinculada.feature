# language: pt
# encoding: iso-8859-1
Funcionalidade: Pesquisa de editora nao vinculada

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado
  Cenario: Validar listagem de editoras nao vinculadas
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    Entao devera apresentar uma lista de editoras nao vinculadas exibindo as colunas
      | coluna_1 | coluna_2 |
      | CNPJ     | Empresa  |

  @finalizado
  Cenario: Validar pesquisa de editoras nao vinculadas por razao social
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    E irei informar no campo pesquisar do popup de vincular editora
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar do popup de vincular editora
    Entao devera retornar o registro especifico pesquisado da editora nao vinculada
      | pesquisar      |
      | Razao Social 1 |

  @finalizado
  Cenario: Validar pesquisa de editoras nao vinculadas por nome fantasia
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    E irei informar no campo pesquisar do popup de vincular editora
      | pesquisar     |
      | nome fantasia |
    E clico no botao pesquisar do popup de vincular editora
    Entao devera retornar o registro especifico pesquisado da editora nao vinculada
      | pesquisar     |
      | nome fantasia |

  @finalizado
  Cenario: Validar pesquisa de editoras nao vinculadas por nome do responsavel
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    E irei informar no campo pesquisar do popup de vincular editora
      | pesquisar        |
      | nome responsavel |
    E clico no botao pesquisar do popup de vincular editora
    Entao devera retornar o registro especifico pesquisado da editora nao vinculada
      | pesquisar        |
      | nome responsavel |

  @finalizado
  Cenario: Validar vincular editora a um objeto
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    E seleciono uma editora para fazer a vinculacao
    E clico no botao vincular do popup de vincular editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar campo obrigatorio editora do vincular editora ao objeto
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E clico no botao vincular editora
    Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa
    E clico no botao vincular do popup de vincular editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                        |
      | o campo editora e de preenchimento obrigatorio. |
