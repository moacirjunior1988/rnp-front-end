# language: pt
# encoding: iso-8859-1
Funcionalidade: Pesquisa de editora vinculada

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu   |
      | edital |

  @finalizado @listagem_exibindo_colunas
  Cenario: Validar listagem de editoras vinculadas colunas
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    Entao devera apresentar uma lista de editoras vinculadas exibindo as colunas
      | coluna_1 | coluna_2 | coluna_3          | coluna_4 |
      | CNPJ     | Empresa  | Preenchimento (%) | Acoes    |

  @finalizado @listagem_opcao_de_pesquisa
  Cenario: Validar listagem de editoras vinculadas opcao de pesquisa
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    Entao devera apresentar uma lista de editoras vinculadas e um campo de pesquisa e um botao de pesquisar

  @finalizado @listagem_exibindo_opcao_com_acesso
  Cenario: Validar listagem de editoras vinculadas coluna acoes opcao com acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Entao devera apresentar uma lista de editoras vinculadas exibindo a coluna acoes
      | Editar | Excluir | Com Acesso | Reiniciar Senha |

  @finalizado @listagem_exibindo_opcao_sem_acesso
  Cenario: Validar listagem de editoras vinculadas coluna acoes opcao sem acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Entao devera apresentar uma lista de editoras vinculadas exibindo a coluna acoes
      | acoes_1 | acoes_2 | acoes_3    |
      | Editar  | Excluir | Sem Acesso |

  @finalizado @pesquisar_razao_social
  Cenario: Validar pesquisa de editoras vinculadas por razao social
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Entao devera retornar o registro especifico pesquisado da editora vinculada
      | pesquisar      |
      | Razao Social 1 |

  @finalizado @pesquisar_nome_fantasia
  Cenario: Validar pesquisa de editoras vinculadas por nome fantasia
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar     |
      | nome fantasia |
    E clico no botao pesquisar da tela de editoras vinculadas
    Entao devera retornar o registro especifico pesquisado da editora vinculada
      | pesquisar     |
      | nome fantasia |

  @finalizado @pesquisar_nome_do_responsavel
  Cenario: Validar pesquisa de editoras vinculadas por nome do responsavel
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar           |
      | nome do responsavel |
    E clico no botao pesquisar da tela de editoras vinculadas
    Entao devera retornar o registro especifico pesquisado da editora vinculada
      | pesquisar           |
      | nome do responsavel |

  @finalizado @reiniciar_senha_com_acesso
  Cenario: Validar pesquisa de editoras vinculadas usando a opcao reiniciar senha com editora com acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Quando apresentar a editora pesquida na tela de editoras vinculadas
    E clico no botao reiniciar senha da tela de editoras vinculadas
    Entao devera apresentar uma mensagem de validacao
      | mensagem                             |
      | Deseja reiniciar a senha do usuario? |
    E clico no botao confirmar do popup de reiniciar senha
    Entao devera apresentar uma mensagem de validacao
      | mensagem                      |
      | Senha reiniciada com sucesso. |

  @finalizado @reiniciar_senha_sem_acesso
  Cenario: Validar pesquisa de editoras vinculadas usando a opcao reiniciar senha com editora sem acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Quando apresentar a editora pesquida na tela de editoras vinculadas
    Entao devera apresentar a tela mais sem a opcao reiniciar senha

  @finalizado @com_acesso
  Cenario: Validar pesquisa de editoras vinculadas usando a opcao com acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Quando apresentar a editora pesquida na tela de editoras vinculadas
    E clico no botao sem acesso/com acesso da tela de editoras vinculadas
      | acao      |
      | comAcesso |
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                 |
      | Deseja permitir o acesso a esta editora? |
    E clico no botao permitir no popup da tela de editoras vinculada
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |

  @finalizado @sem_acesso
  Cenario: Validar pesquisa de editoras vinculadas usando a opcao sem acesso
    Dado que estou na tela de listagem de edital com editais e objetos cadastrados
    E clico no botao cadastrar objeto em um edital ja cadastrado
    Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas
    E clico na aba editoras
    Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto
    E irei informar no campo pesquisar da tela de editoras vinculadas
      | pesquisar      |
      | Razao Social 1 |
    E clico no botao pesquisar da tela de editoras vinculadas
    Quando apresentar a editora pesquida na tela de editoras vinculadas
    E clico no botao sem acesso/com acesso da tela de editoras vinculadas
      | acao      |
      | semAcesso |
    Entao devera apresentar uma mensagem de validacao
      | mensagem                               |
      | Deseja bloquear acesso a esta editora? |
    E clico no botao permitir no popup da tela de editoras vinculada
    Entao devera apresentar uma mensagem de validacao
      | mensagem                       |
      | Registro alterado com sucesso. |
