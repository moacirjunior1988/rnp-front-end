# language: pt
# encoding: iso-8859-1
Funcionalidade: Pesquisa e lista de solicitacoes de cadastramento de editoras

  Contexto: Logar no sistema PNLD Digital
    Dado que acesso a tela de login do sistema pnld digital
    Quando informar um usuario e a senha validos
      | usuario                                  | senha        |
      | andre.dellatorre+83464047865@redspark.io | Asdf!2345678 |
    E clico no botao entrar
    Entao devera apresentar a tela home do sistema
    E acesso o menu
      | menu                         |
      | solicitacao cadastro editora |

  @finalizado
  Cenario: Validar listagem das solicidacoes de cadastramento das editoras
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    Entao devera apresentar as colunas na lista de solicitacoes de cadastramento de editoras
      | coluna_1 | coluna_2 | coluna_3            | coluna_4 | coluna_5 |
      | CNPJ     | Empresa  | Data da Solicitacao | Situacao | Acoes    |

  @finalizado
  Cenario: Validar aprovacao de uma solicitacoes de cadastramento de editora
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no botao validar de uma das solicitacoes de cadastramento de editora
    Quando apresentar um popup com as opcoes de aprovar/reprovar e o campo justificativa
    E clico na opcao aprovar/reprovar do popup de solicitacoes de cadastramento de editoras
      | opcao   |
      | aprovar |
    E clico no botao confirmar do popup de solicitacoes de cadastramento de editoras
    Entao devera voltar para a tela de listagem com a editora no filtro de aprovado/reprovado
      | opcao    |
      | Aprovado |

  @finalizado
  Cenario: Validar reprovacao de uma solicitacoes de cadastramento de editora
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no botao validar de uma das solicitacoes de cadastramento de editora
    Quando apresentar um popup com as opcoes de aprovar/reprovar e o campo justificativa
    E clico na opcao aprovar/reprovar do popup de solicitacoes de cadastramento de editoras
      | opcao    |
      | reprovar |
    E informarei o campo justificativa do popup de solicitacoes de cadastramento de editoras
      | justificativa |
      | Teste 123     |
    E clico no botao confirmar do popup de solicitacoes de cadastramento de editoras
    Entao devera voltar para a tela de listagem com a editora no filtro de aprovado/reprovado
      | opcao      |
      | Reprovados |

  @finalizado
  Cenario: Validar reprovacao de uma solicitacoes de cadastramento de editora sem informar justificativa
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no botao validar de uma das solicitacoes de cadastramento de editora
    Quando apresentar um popup com as opcoes de aprovar/reprovar e o campo justificativa
    E clico na opcao aprovar/reprovar do popup de solicitacoes de cadastramento de editoras
      | opcao    |
      | reprovar |
    E clico no botao confirmar do popup de solicitacoes de cadastramento de editoras
    Entao devera apresentar uma mensagem de validacao
      | mensagem                                              |
      | o campo justificativa e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar filtros da tela de listagem das solicidacoes de cadastramento das editoras
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras
      | filtro |
      | Todos  |
    Entao devera apresentar todas as solicitacoes com todas as situacoes
    E clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras
      | filtro   |
      | Aprovado |
    Entao devera apresentar somente as solicitacoes filtradas pelo filtro selecionado
    E clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras
      | filtro     |
      | Reprovados |
    Entao devera apresentar somente as solicitacoes filtradas pelo filtro selecionado
    E clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras
      | filtro        |
      | Nao Validados |
    Entao devera apresentar somente as solicitacoes filtradas pelo filtro selecionado

  @finalizado
  Cenario: Validar pesquisa de solicitacoes de cadastramento de editora por CNPJ
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar |
      | CNPJ      |
    E clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora
    Entao devera retornar os registros especificos pesquisados na tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar |
      | CNPJ      |

  @finalizado
  Cenario: Validar pesquisa de solicitacoes de cadastramento de editora por razao social
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar    |
      | razao social |
    E clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora
    Entao devera retornar os registros especificos pesquisados na tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar    |
      | razao social |

  @finalizado
  Cenario: Validar o historico da editora que abriu a solicitacao de cadastro
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no botao historico de uma das solicitacoes de cadastramento de editora
    Quando apresentar a grid com o historico da solicitacao do cadastramento da editora
    Entao devera apresentar os devidos campos na grid de historico
      | coluna_1 | coluna_2 | coluna_3            | coluna_4    | coluna_5        | coluna_6 | coluna_7                    |
      | Empresa  | Edital   | Data da Solicitacao | Responsavel | Data da Analise | Situacao | Justificativa da Reprovacao |

  @finalizado
  Cenario: Validar pesquisa de solicitacoes de cadastramento de editora por CNPJ nao cadastrado
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar           |
      | CNPJ nao cadastrado |
    E clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                    |
      | Nenhum registro encontrado. |

  @finalizado
  Cenario: Validar pesquisa de solicitacoes de cadastramento de editora por razao social nao cadastrado
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora
      | pesquisar                   |
      | razao social nao cadastrado |
    E clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora
    Entao devera apresentar uma mensagem de validacao
      | mensagem                    |
      | Nenhum registro encontrado. |
      
 	@finalizado
  Cenario: Validar o modal de justificativa do historico de reprovacao
    Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras
    E clico no botao historico de uma das solicitacoes de cadastramento de editora
    Quando apresentar a grid com o historico da solicitacao do cadastramento da editora
    E clico no botao justificativa da solicitacao do cadastramento da editora
    Entao devera apresentar a modal com a justificativa daquele registro de reprovacao