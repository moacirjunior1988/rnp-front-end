# language: pt
# encoding: iso-8859-1
Funcionalidade: Solicitacao de cadastro de editora

  @finalizado
  Cenario: Validar solicitacao de cadastro de editora com dados validos
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar solicitacao de cadastro de editora nao informando nome fantasia
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     |              | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                       |
      | Registro incluido com sucesso. |

  @finalizado
  Cenario: Validar campo obrigatorio cnpj da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      |      | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                    |
      | O campo cpf e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio razao social da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir |             | inserir      | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                             |
      | O campo razao social e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio cpf do responsavel da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      |                | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                                |
      | O campo cpf responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio nome do responsavel da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | inserir        |                 | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                                    |
      | O campo nome do responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio telefone do responsavel da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | inserir        | inserir         |                     | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                                        |
      | O campo telefone do responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio email do responsavel da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | inserir        | inserir         | inserir             |                  | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                                     |
      | O campo email do responsavel e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar campo obrigatorio vincular edital ao objeto da solicitacao de cadastro de editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          |                      |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras
      | mensagem                                                          |
      | O campo vincular edital ao objeto e de preenchimento obrigatorio. |

  @finalizado
  Cenario: Validar solicitacao de cadastro de editora com cnpj invalido
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj     | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | invalido | inserir     | inserir      | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                      |
      | O campo cnpj deve ser valido. |

  @finalizado
  Cenario: Validar solicitacao de cadastro de editora com cpf do responsavel invalido
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | inserir | inserir     | inserir      | invalido       | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                                    |
      | O campo cpf do responsavel deve ser valido. |

  @finalizado
  Cenario: Validar regra de duplicidade ao solicitar um cadastro de uma nova editora
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj          | razaoSocial   | nomeFantasia  | cpfResponsavel | nomeResponsavel | telefoneResponsavel | emailResponsavel | vincularEditalObjeto |
      | ja cadastrado | ja cadastrado | ja cadastrado | inserir        | inserir         | inserir             | inserir          | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                                     |
      | O CNPJ informado possui cadastro no sistema. |

  @finalizado
  Cenario: Validar solicitacao de cadastro de editora com responsavel ja cadastrado no sistema
    Dado que acesso a tela de login do sistema pnld digital
    E clico no link de solicitar acesso
    Quando apresentar a tela solicitacao de cadastro de editora
    E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora
      | cnpj    | razaoSocial | nomeFantasia | cpfResponsavel            | nomeResponsavel           | telefoneResponsavel       | emailResponsavel          | vincularEditalObjeto |
      | inserir | inserir     | inserir      | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado | responsavel ja cadastrado | inserir              |
    E clico no botao solicitar da tela de solicitacao de cadastro de editora
    Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras
      | mensagem                                                                                   |
      | O CPF informado possui cadastro no sistema. O usuario sera direcionado para tela de login. |
    E sou redirecionado para a tela de login do sistema
