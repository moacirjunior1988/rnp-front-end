package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class categoriaPO extends seleniumActions
{
	//Elements
	private static WebElement btnAbaCategoria() { return GetElement(byElements.Id, "teste"); }
	private static WebElement gridCategorias() { return GetElement(byElements.Id, "teste"); }
	private static WebElement btnCadastrarCategoria() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtNome() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtCodigo() { return GetElement(byElements.Id, "teste"); }
	private static WebElement btnCadastrar() { return GetElement(byElements.Id, "teste"); }
	
	//Sendkeys
	public static void nome(String text)
	{
		if(text.length() > 0) Sendkeys(txtNome(), text);
		else Clear(txtNome());
	}
	
	public static void codigo(String text)
	{
		if(text.length() > 0) Sendkeys(txtCodigo(), text);
		else Clear(txtCodigo());
	}
	
	//Clicks
	public static void abaCategoria()
	{
		Clicks(btnAbaCategoria());
	}
	
	public static void cadastrarCategoria()
	{
		Clicks(btnCadastrarCategoria());
	}
	
	public static void cadastrar()
	{
		Clicks(btnCadastrar());
	}
	
	//Valida��o de exibi��o da tela categoria
	public static boolean validationOfExibitionOfGridCategorias()
	{
		return ValidacoesDeElementos(gridCategorias());
	}
}
