package pageObject;

import org.openqa.selenium.WebElement;
import enums.byElements;
import geral.seleniumActions;

public class editalPO extends seleniumActions
{
	private static WebElement txtTituloEdital() { return GetElement(byElements.Name, "title"); }
	private static WebElement txtAnoReferencia() { return GetElement(byElements.Name, "year"); }
	private static WebElement cbxTipoEdital() { return GetElement(byElements.Name, "type"); }
	private static WebElement txtNumeroEdital() { return GetElement(byElements.Name, "number"); }
	private static WebElement btnCancelarOEdital() { return GetElement(byElements.XPath, "//button[text()='Cancelar']"); }
	private static WebElement btnCadastrarEdital() { return GetElement(byElements.XPath, "//button[text()='Cadastrar']"); }
	private static WebElement btnAlterarEdital() { return GetElement(byElements.XPath, "//button[text()='Alterar']"); }
	private static WebElement pgCadastrarEdital() { return GetElement(byElements.XPath, "/html/body/div[2]/div[3]/div/form/h2"); }
	private static WebElement msgTituloEdital;
	private static WebElement msgAnoReferencia;
	private static WebElement msgTipoEdital;
	private static WebElement msgNumeroEdital;
	
	//Sendkeys
	public static void tituloEdital(String text)
	{
		if(text.length() > 0) Sendkeys(txtTituloEdital(), text);
		else Clear(txtTituloEdital());
	}
	
	public static void anoReferencia(String text)
	{
		if(text.length() > 0) Sendkeys(txtAnoReferencia(), text);
		else Clear(txtAnoReferencia());
	}
	
	public static void numeroEdital(String text)
	{
		if(text.length() > 0) Sendkeys(txtNumeroEdital(), text);
		else Clear(txtNumeroEdital());
	}
	
	//Combobox
	public static void tipoEdital(String text)
	{
		ComboBoxSelect(cbxTipoEdital(), text);
	}
	
	//Click
	public static void cadastrarOEdital()
	{
		Clicks(btnCadastrarEdital());
	}
	
	public static void alterarOEdital()
	{
		Clicks(btnAlterarEdital());
	}
	
	public static void cancelarEdital()
	{
		Clicks(btnCancelarOEdital());
	}
	
	// Validação da exibição de tela
	public static boolean validationOfExibitionOfPageInsertEdital()
	{
		return ValidacoesDeElementos(pgCadastrarEdital());
	}
	
	//Validação de exibição de mensagens de obrigatóriedade
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeTituloEdital()
	{
		msgTituloEdital = GetElement(byElements.XPath, "/html/body/div[2]/div[3]/div/form/div[1]/div/span");
		return Text(msgTituloEdital);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoReferencia()
	{
		msgAnoReferencia = GetElement(byElements.XPath, "/html/body/div[2]/div[3]/div/form/div[2]/div[1]/div/span");
		return Text(msgAnoReferencia);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeTipoEdital()
	{
		msgTipoEdital = GetElement(byElements.XPath, "/html/body/div[2]/div[3]/div/form/div[2]/div[1]/div/span");
		return Text(msgTipoEdital);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeNumeroEdital()
	{
		msgNumeroEdital = GetElement(byElements.XPath, "/html/body/div[2]/div[3]/div/form/div[2]/div[3]/div/span");
		return Text(msgNumeroEdital);
	}
}
