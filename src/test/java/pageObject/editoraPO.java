package pageObject;

import java.util.List;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class editoraPO extends seleniumActions
{
	private static WebElement txtPesquisa = GetElement(byElements.Id, "teste");
	private static WebElement btnPesquisar = GetElement(byElements.Id, "teste");
	private static WebElement btnVincularEditora = GetElement(byElements.Id, "teste");
	private static WebElement btnCadastrarEditora = GetElement(byElements.Id, "teste");
	private static WebElement txtCNPJ = GetElement(byElements.Id, "teste");
	private static WebElement txtRazaoSocial = GetElement(byElements.Id, "teste");
	private static WebElement txtNomeFantasia = GetElement(byElements.Id, "teste");
	private static WebElement txtCPFResponsavel = GetElement(byElements.Id, "teste");
	private static WebElement txtNomeResponsavel = GetElement(byElements.Id, "teste");
	private static WebElement txtTelefoneResponsavel = GetElement(byElements.Id, "teste");
	private static WebElement txtEmailResponsavel = GetElement(byElements.Id, "teste");
	private static WebElement btnCadastrarAEditora = GetElement(byElements.Id, "teste");
	private static WebElement txtPesquisaEditorasNaoVinculadas = GetElement(byElements.Id, "teste");
	private static WebElement btnPesquisarEditorasNaoVinculadas = GetElement(byElements.Id, "teste");
	private static WebElement clnPoppupNaoVinculadoCNPJ = GetElement(byElements.Id, "teste");
	private static WebElement clnPoppupNaoVinculadoEmpresa = GetElement(byElements.Id, "teste");
	private static WebElement rgtEditoraPesquisadaNaoVinculada = GetElement(byElements.Id, "teste");
	private static List<WebElement> listaEditorasNaoVinculadas = GetElements(byElements.Id, "teste");
	private static WebElement btnVincularEditoraAoObjeto = GetElement(byElements.Id, "teste");
	private static WebElement clnTelaPesquisaEditorasVinculadasCNPJ = GetElement(byElements.Id, "teste");
	private static WebElement clnTelaPesquisaEditorasVinculadasRazaoSocial = GetElement(byElements.Id, "teste");
	private static WebElement clnTelaPesquisaEditorasVinculadasPreenchimentoPercentual = GetElement(byElements.Id, "teste");
	private static WebElement clnTelaPesquisaEditorasVinculadasAcoes = GetElement(byElements.Id, "teste");
	private static WebElement btnEditar = GetElement(byElements.Id, "teste");
	private static WebElement btnExcluir = GetElement(byElements.Id, "teste");
	private static WebElement btnComAcesso = GetElement(byElements.Id, "teste");
	private static WebElement btnSemAcesso = GetElement(byElements.Id, "teste");
	private static WebElement btnReiniciarSenha = GetElement(byElements.Id, "teste");
	private static List<WebElement> listaEditorasVinculadas = GetElements(byElements.Id, "teste");
	private static WebElement btnConfirmarReiniciarSenha = GetElement(byElements.Id, "teste");
	private static WebElement btnPermitirNaoPermitirAcesso = GetElement(byElements.Id, "teste");
	
	//Sendkeys
	public static void pesquisa(String text)
	{
		if(text.length() > 0) Sendkeys(txtPesquisa, text);
		else Clear(txtPesquisa);
	}
	
	public static void cnpj(String text)
	{
		if(text.length() > 0) Sendkeys(txtCNPJ, text);
		else Clear(txtCNPJ);
	}
	
	public static void razaoSocial(String text)
	{
		if(text.length() > 0) Sendkeys(txtRazaoSocial, text);
		else Clear(txtRazaoSocial);
	}
	
	public static void nomeFantasia(String text)
	{
		if(text.length() > 0) Sendkeys(txtNomeFantasia, text);
		else Clear(txtNomeFantasia);
	}
	
	public static void cpfResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtCPFResponsavel, text);
		else Clear(txtCPFResponsavel);
	}
	
	public static void nomeResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtNomeResponsavel, text);
		else Clear(txtNomeResponsavel);
	}
	
	public static void telefoneResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtTelefoneResponsavel, text);
		else Clear(txtTelefoneResponsavel);
	}
	
	public static void emailResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtEmailResponsavel, text);
		else Clear(txtEmailResponsavel);
	}
	
	public static void pesquisaEditorasNaoVinculadas(String text)
	{
		Sendkeys(txtPesquisaEditorasNaoVinculadas, text);
	}
	
	//Click
	public static void pesquisar()
	{
		Clicks(btnPesquisar);
	}
	
	public static void vincularEditora()
	{
		Clicks(btnVincularEditora);
	}
	
	public static void cadastrarEditora()
	{
		Clicks(btnCadastrarEditora);
	}
	
	public static void cadastrarAEditora()
	{
		Clicks(btnCadastrarAEditora);
	}
	
	public static void pesquisarEditorasNaoVinculadas()
	{
		Clicks(btnPesquisarEditorasNaoVinculadas);
	}
	
	public static void selecionarEditoraParaVinculadacao(int qtd)
	{
		for(int a = 0; a <= (listaEditorasNaoVinculadas.size() - 1); a++)
		{
			if(a < qtd) ClicksJS(listaEditorasNaoVinculadas.get(a));
			else break;
		}
	}
	
	public static void vincularEditoraAoObjeto()
	{
		Clicks(btnVincularEditoraAoObjeto);
	}
	
	public static void reiniciarSenha()
	{
		Clicks(btnReiniciarSenha);
	}
	
	public static void confirmarReiniciarSenha()
	{
		Clicks(btnConfirmarReiniciarSenha);
	}
	
	public static void comAcesso()
	{
		Clicks(btnComAcesso);
	}
	
	public static void semAcesso()
	{
		Clicks(btnSemAcesso);
	}
	
	public static void permitirOuNaoPermitir()
	{
		Clicks(btnPermitirNaoPermitirAcesso);
	}
	
	//Valida��o de exibi��o da tela de editoras
	public static boolean validacaoDeExibicaoDaTelaDeEditoras()
	{
		return ValidacoesDeElementos(txtPesquisa);
	}
	
	//Valida��o de popup de editoras n�o vinculadas
	public static boolean validacaoDeExibicaoDoPopupDeEditorasNaoVinculadas()
	{
		return ValidacoesDeElementos(txtPesquisaEditorasNaoVinculadas);
	}

	//Valida��o de exibi��o das colunas no popup de editoras n�o vinculadas
	public static boolean validacaoDeExibicaoDaColunaDeCNPJNoPopupDeEditorasNaoVinculadas()
	{
		return ValidacoesDeElementos(clnPoppupNaoVinculadoCNPJ);
	}
	
	public static boolean validacaoDeExibicaoDaColunaDeEmpresaNoPopupDeEditorasNaoVinculadas()
	{
		return ValidacoesDeElementos(clnPoppupNaoVinculadoEmpresa);
	}
	
	//Retornar a primeira linha na primeira coluna Empresa das editoras n�o vinculadas
	public static String ReturnarRazaoSocialDaEditoraPesquisadaNoPopupDeEditorasNaoVinculadas()
	{
		return Text(rgtEditoraPesquisadaNaoVinculada);
	}
	
	//Valida��o da exibi��o das colunas da tela de editoras j� vinculadas
	public static String validacaoDeExibicaoDaColunaDeCNPJNaTelaDePesquisaDeEditorasVinculadas()
	{
		return Text(clnTelaPesquisaEditorasVinculadasCNPJ);
	}
	
	public static String validacaoDeExibicaoDaColunaDeRazaoSocialNaTelaDePesquisaDeEditorasVinculadas()
	{
		return Text(clnTelaPesquisaEditorasVinculadasRazaoSocial);
	}
	
	public static String validacaoDeExibicaoDaColunaDePreenchimentoPercentualNaTelaDePesquisaDeEditorasVinculadas()
	{
		return Text(clnTelaPesquisaEditorasVinculadasPreenchimentoPercentual);
	}
	
	public static String validacaoDeExibicaoDaColunaDeAcoesNaTelaDePesquisaDeEditorasVinculadas()
	{
		return Text(clnTelaPesquisaEditorasVinculadasAcoes);
	}
	
	//Validar exibi��o campo e bot�o pesquisar editoras vinculadas
	public static boolean validacaoExibicaoCampoPesquisarEditorasVinculadas()
	{
		return ValidacoesDeElementos(txtPesquisa);
	}
	
	public static boolean validacaoExibicaoBotaoPesquisarEditorasVinculadas()
	{
		return ValidacoesDeElementos(btnPesquisar);
	}
	
	//Valida��o das a��es da editora com acesso
	public static String validacaoDasAcoesDaEditoraComAcessoEditar()
	{
		return Text(btnEditar);
	}
	
	public static String validacaoDasAcoesDaEditoraComAcessoExcluir()
	{
		return Text(btnExcluir);
	}
	
	public static String validacaoDasAcoesDaEditoraComAcesso()
	{
		return Text(btnComAcesso);
	}
	
	public static String validacaoDasAcoesDaEditoraComAcessoReiniciarSenha()
	{
		return Text(btnReiniciarSenha);
	}
	
	//Valida��o de retorno pesquisa editoras vinculadas
	public static String validacaoDeRetornoPesquisaEditorasVinculadas()
	{
		return Text(listaEditorasVinculadas.get(1));
	}
}
