package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class homePO extends seleniumActions
{
	private static WebElement btnMenuEdital() { return GetElement(byElements.Id, "teste"); }
	private static WebElement pgListagemEdital() { return GetElement(byElements.CssSelector, ""); }
	private static WebElement pgListagemSolicitacaoCadastramentoEditora() { return GetElement(byElements.XPath, "Teste"); }
	
	//Click
	public static void menuEdital() 
	{
		Clicks(btnMenuEdital());
	}
	
	//Valida��o de exibi��o de tela
	public static boolean validationOfExibitionOfPageListagemEdital()
	{
		return ValidacoesDeElementos(pgListagemEdital());
	}
	
	public static boolean validationOfExibitionOfPageListagemSolicitacaoCadastramentoEditora()
	{
		return ValidacoesDeElementos(pgListagemSolicitacaoCadastramentoEditora());
	}
}
