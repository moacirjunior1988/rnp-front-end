package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

import java.util.List;

public class loginPO extends seleniumActions
{
	private static WebElement txtUser() { return GetElement(byElements.Name, "username"); }
	private static WebElement txtPassword() { return GetElement(byElements.Name, "password"); }
	private static WebElement iframeRecaptche() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div/div[2]/form/div/div[3]/div/div/div/div/div/iframe"); }
	private static WebElement btnReCaptcha() { return GetElement(byElements.Id, "recaptcha-anchor"); }
	private static WebElement btnEntrar() { return GetElement(byElements.Id, "//button[text()='Entrar']"); }
	private static WebElement pgHome() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div/div[2]/div[1]/div/h4"); }

	//Sendkeys
	public static void user(String text)
	{
		Sendkeys(txtUser(), text);
	}
	
	public static void password(String text)
	{
		Sendkeys(txtPassword(), text);
	}
	
	//Click
	public static void entrar() 
	{
		Clicks(btnEntrar());
	}

	public static void recaptche()
	{
		SelecionaIFrame(iframeRecaptche());
		ClicksJS(btnReCaptcha());
	}

	//Valida��o de exibi��o de tela
	public static boolean validationOfExibitionOfPage()
	{
		return ValidacoesDeElementos(pgHome());
	}
}
