package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class mensagensCategoriaPO extends seleniumActions
{
	//Elements
	private static WebElement msgExibidaCadatrarAlterarCategoria() { return GetElement(byElements.XPath, "Teste"); }
	
	//Retorno mensagem exibida
	public static String returnTextMensagemCadastrarAlterarDuplicidadeCategoria()
	{
		return Text(msgExibidaCadatrarAlterarCategoria());
	}
}
