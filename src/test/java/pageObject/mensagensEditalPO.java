package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class mensagensEditalPO extends seleniumActions
{
	//Elements
	private static WebElement msgExibidaCadatrarAlterarEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]"); }
	
	//Retorno mensagem exibida
	public static String returnTextMensagemCadastrarEdital()
	{
		return Text(msgExibidaCadatrarAlterarEdital());
	}
	
	public static String returnTextMensagemAlterarEdital()
	{
		return Text(msgExibidaCadatrarAlterarEdital());
	}
	
	public static String returnTextMensagemDuplicadoEdital()
	{
		return Text(msgExibidaCadatrarAlterarEdital());
	}
}
