package pageObject;

import enums.byElements;
import geral.seleniumActions;
import org.openqa.selenium.WebElement;

public class mensagensLoginPO extends seleniumActions
{
    private static WebElement msgObrigatoriedadeCPFEmail() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div/div[2]/form/div/div[1]/div/p"); }
    private static WebElement msgObrigatoriedadeSenha() { return GetElement(byElements.XPath,"//*[@id=\"root\"]/div/div/div[2]/form/div/div[2]/div/p");}
    private static WebElement msgInvalidesCpfEmailESenha() { return GetElement(byElements.Id, "notistack-snackbar"); }

    //Retorno mensagem obrigatoriedade
    public static String returnTextMensagemObrigatoriedadeCPFEmail()
    {
        return Text(msgObrigatoriedadeCPFEmail());
    }

    public static String returnTextMensagemObrigatoriedadeSenha()
    {
        return Text(msgObrigatoriedadeSenha());
    }

    // Retorno mensagem invalides
    public static String returnTextMensagemInvalidesCPFEmailESenha()
    {
        return Text(msgInvalidesCpfEmailESenha());
    }
}
