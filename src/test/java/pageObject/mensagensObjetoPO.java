package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class mensagensObjetoPO extends seleniumActions
{
	//Elements
	private static WebElement msgExibidaCadatrarAlterarObjeto() { return GetElement(byElements.XPath, "Teste"); }
	
	//Retorno mensagem exibida
	public static String returnTextMensagemCadastrarAlterarDuplicidadeObjeto()
	{
		return Text(msgExibidaCadatrarAlterarObjeto());
	}
}
