package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class objetoPO extends seleniumActions
{
	private static WebElement pgCadastrarObjeto() { return GetElement(byElements.Name, "name"); }
	private static WebElement txtNomeObjeto() { return GetElement(byElements.Name, "name"); }
	private static WebElement txtCodigoObjeto() { return GetElement(byElements.Name, "number"); }
	private static WebElement ckbHabilitarCadastroObrasSim() { return GetElement(byElements.Name, "enabledRegister"); }
	private static WebElement ckbHabilitarCadastroObrasNao() { return GetElement(byElements.Name, "enabledRegister"); }
	private static WebElement txtDataHoraAbertura() { return GetElement(byElements.Name, "openingDate"); }
	private static WebElement txtDataHorafechamento() { return GetElement(byElements.Name, "closeDate"); }
	private static WebElement ckbHabilitarFasesInscricaoDasEditoras() { return GetElement(byElements.Name, "enabledPublisherRegister"); }
	private static WebElement ckbHabilitarFasesValidacaoDaInscricao() { return GetElement(byElements.Name, "enabledRegisterValidation"); }
	private static WebElement ckbHabilitarFasesAnaliseDeTributosFisicos() { return GetElement(byElements.Name, "enabledAnalysisPhysicalAttributes"); }
	private static WebElement ckbHabilitarFasesHabilitacao() { return GetElement(byElements.Name, "enabledQualification"); }
	private static WebElement ckbHabilitarFasesProcessamento() { return GetElement(byElements.Name, "enabledProcessing"); }
	private static WebElement ckbHabilitarFasesNegociacao() { return GetElement(byElements.Name, "enabledNegotiation"); }
	private static WebElement ckbHabilitarFasesContratacao() { return GetElement(byElements.Name, "enabledHiring"); }
	private static WebElement ckbHabilitarFasesProducaoEPostagem() { return GetElement(byElements.Name, "enabledProductionPostage"); }
	private static WebElement ckbHabilitarFasesDistribuicao() { return GetElement(byElements.Name, "enabledDistribution"); }
	private static WebElement ckbHabilitarFasesControleDeQualidade() { return GetElement(byElements.Name, "enabledQualityControl"); }
	private static WebElement ckbHabilitarFasesMonitoramentoEAvaliacao() { return GetElement(byElements.Name, "enabledMonitoring"); }
	private static WebElement txtDataInicalPeriodoInscricaoEntrada() { return GetElement(byElements.Name, "registerStartDate"); }
	private static WebElement txtDataFinalPeriodoInscricaoEntrada() { return GetElement(byElements.Name, "registerEndDate"); }
	private static WebElement txtDataInicialTriagem() { return GetElement(byElements.Name, "screeningStartDate"); }
	private static WebElement txtDataFinalTriagem() { return GetElement(byElements.Name, "screeningEndDate"); }
	private static WebElement txtAnoAtendimento() { return GetElement(byElements.Name, "serviceYears"); }
	private static WebElement ckbSituacaoObrasAprovado() { return GetElement(byElements.Name, "showQualificationApproved"); }
	private static WebElement ckbSituacaoObrasReprovado() { return GetElement(byElements.Name, "showQualificationReproved"); }
	private static WebElement ckbSituacaoObrasAprovadaCondicionada() { return GetElement(byElements.Name, "showQualificationConditionalApproved"); }
	private static WebElement txtLimiteObras() { return GetElement(byElements.Name, "itemsLimit"); }
	private static WebElement txtDataPublicacaoDou() { return GetElement(byElements.Name, "publishDate"); }
	private static WebElement btnSalvar() { return GetElement(byElements.XPath, "//button[text()='Salvar']"); }
	private static WebElement abaComplemento() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[1]/div/div[2]"); }
	private static WebElement abaEditoras() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[1]/div/div[3]"); }
	private static WebElement abaEstrutura() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[1]/div/div[4]"); }
	private static WebElement abaOrganizacao() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[1]/div/div[5]"); }
	private static WebElement msgNomeObjeto;
	private static WebElement msgCodigoObjeto;
	private static WebElement msgDataHoraAbertura;
	private static WebElement msgDataHoraFechamento;
	private static WebElement msgDataInicalPeriodoInscricaoEntrada;
	private static WebElement msgDataFinalPeriodoInscricaoEntrada;
	private static WebElement msgDataInicialTriagem;
	private static WebElement msgDataFinalTriagem;
	private static WebElement msgAnoAtendimento;
	private static WebElement msgLimiteObras;
	private static WebElement msgDataPublicacaoDou;
	
	//Sendkeys
	public static void nomeObjeto(String text)
	{
		if(text.length() > 0) Sendkeys(txtNomeObjeto(), text);
		else Clear(txtNomeObjeto());
	}
	
	public static void codigoObjeto(String text)
	{
		if(text.length() > 0) Sendkeys(txtCodigoObjeto(), text);
		else Clear(txtCodigoObjeto());
	}
	
	public static void dataHoraAbertura(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataHoraAbertura(), text);
		else ClearValue(txtDataHoraAbertura());
	}
	
	public static void dataHorafechamento(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataHorafechamento(), text);
		else ClearValue(txtDataHorafechamento());
	}
	
	public static void dataInicialPeriodoInscricaoEntrada(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataInicalPeriodoInscricaoEntrada(), text);
		else ClearValue(txtDataInicalPeriodoInscricaoEntrada());
	}
	
	public static void dataFinalPeriodoInscricaoEntrada(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataFinalPeriodoInscricaoEntrada(), text);
		else ClearValue(txtDataFinalPeriodoInscricaoEntrada());
	}
	
	public static void dataInicialTriagem(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataInicialTriagem(), text);
		else ClearValue(txtDataFinalPeriodoInscricaoEntrada());
	}
	
	public static void dataFinalTriagem(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataFinalTriagem(), text);
		else ClearValue(txtDataFinalTriagem());
	}
	
	public static void anoAtendimento(String text)
	{
		if(text.length() > 0) Sendkeys(txtAnoAtendimento(), text);
		else Clear(txtAnoAtendimento());
	}
	
	public static void limiteObras(String text)
	{
		if(text.length() > 0) Sendkeys(txtLimiteObras(), text);
		else Clear(txtLimiteObras());
	}
	
	public static void dataPublicacaoDou(String text)
	{
		if(text.length() > 0) Sendkeys(txtDataPublicacaoDou(), text);
		else ClearValue(txtDataPublicacaoDou());
	}
	
	//Clicks
	public static void salvar()
	{
		Clicks(btnSalvar());
	}

	public static void abaComplementos()
	{
		Clicks(abaComplemento());
	}
	
	public static void abaEditora()
	{
		Clicks(abaEditoras());
	}
	
	public static void abaCategoria()
	{
		Clicks(abaEstrutura());
	}
	
	public static void abaOrganizcoes()
	{
		Clicks(abaOrganizacao());
	}
	
	//Checkbox
	public static void habilitarCadastroObras(String text)
	{
		if(text.toLowerCase().equals("sim")) ClicksJS(ckbHabilitarCadastroObrasSim());
		else ClicksJS(ckbHabilitarCadastroObrasNao());
	}

	public static void habilitarFases(String text)
	{
		String[] listaFases = text.split(",");
		
		for(int a = 0; a <= (listaFases.length - 1); a++)
		{
			switch (listaFases[a].toUpperCase()) 
			{
				case "1"://INSCRI��O DAS EDITORAS
						ClicksJS(ckbHabilitarFasesInscricaoDasEditoras());
					break;
					
				case "2"://VALIDA��O DA INSCRI��O
						ClicksJS(ckbHabilitarFasesValidacaoDaInscricao());
					break;
						
				case "3"://AN�LISE DE TRIBUTOS F�SICOS
						ClicksJS(ckbHabilitarFasesAnaliseDeTributosFisicos());
					break;
					
				case "4"://HABILITA��O
						ClicksJS(ckbHabilitarFasesHabilitacao());
					break;
					
				case "5"://PROCESSAMENTO
						ClicksJS(ckbHabilitarFasesProcessamento());
					break;
					
				case "6"://NEGOCIA��O
						ClicksJS(ckbHabilitarFasesNegociacao());
					break;
					
				case "7"://CONTRATA��O
						ClicksJS(ckbHabilitarFasesContratacao());
					break;
					
				case "8"://PRODU��O E POSTAGEM
						ClicksJS(ckbHabilitarFasesProducaoEPostagem());
					break;
					
				case "9"://DISTRIBUI��O
						ClicksJS(ckbHabilitarFasesDistribuicao());
					break;
					
				case "10"://CONTROLE DE QUALIDADE
						ClicksJS(ckbHabilitarFasesControleDeQualidade());
					break;
					
				case "11"://MONITORAMENTO E AVALIA��O
						ClicksJS(ckbHabilitarFasesMonitoramentoEAvaliacao());
					break;
			}
		}
	}
	
	public static void situacaoObras(String text)
	{
		switch (text.toUpperCase()) 
		{
			case "APROVADO":
					ClicksJS(ckbSituacaoObrasAprovado());
				break;
	
			case "REPROVADO":
					ClicksJS(ckbSituacaoObrasReprovado());
				break;
				
			case "APROVADO CONDICIONADA":
					ClicksJS(ckbSituacaoObrasAprovadaCondicionada());
				break;
		}
	}
	
	
	//Valida��o de exibi��o de tela
	public static boolean validationOfExibitionOfPageCadastrarObjeto()
	{
		return ValidacoesDeElementos(pgCadastrarObjeto());
	}
	
	//Valida��o de exibi��o das abas da tela de objeto
	public static boolean validationOfExibitionOfAbaComplemento()
	{
		return ValidacoesDeElementos(abaComplemento());
	}
	
	public static boolean validationOfExibitionOfAbaEditoras()
	{
		return ValidacoesDeElementos(abaEditoras());
	}
	
	public static boolean validationOfExibitionOfAbaEstrutura()
	{
		return ValidacoesDeElementos(abaEstrutura());
	}
	
	public static boolean validationOfExibitionOfAbaOrganizacao()
	{
		return ValidacoesDeElementos(abaOrganizacao());
	}
	
	//Valida��o de campos preenchidos para edi��o.
	public static boolean validacaoDeCampoPreenchidoDoEditarObjeto()
	{
		if(!Text(txtNomeObjeto()).equals("")) return true;
		else return false;
	}
	
	//Valida��o de exibi��o de mensagens de obrigat�riedade
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeNomeObjetivo()
	{
		msgNomeObjeto = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[2]/div[1]/div/span");
		return Text(msgNomeObjeto);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeCodigoObjetivo()
	{
		msgCodigoObjeto = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[2]/div[2]/div/span");
		return Text(msgCodigoObjeto);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataHoraAbertura()
	{
		msgDataHoraAbertura = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[4]/div[1]/div[2]/span");
		System.out.println("Mensagem: " + msgDataHoraAbertura);
		return Text(msgDataHoraAbertura);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataHoraFechamento()
	{
		msgDataHoraFechamento = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[4]/div[3]/div[2]/span");
		return Text(msgDataHoraFechamento);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataInicalPeriodoInscricaoEntrada()
	{
		msgDataInicalPeriodoInscricaoEntrada = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[1]/div[1]/div[2]/span");
		return Text(msgDataInicalPeriodoInscricaoEntrada);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataFinalPeriodoInscricaoEntrada()
	{
		msgDataFinalPeriodoInscricaoEntrada = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[1]/div[2]/div[2]/span");
		return Text(msgDataFinalPeriodoInscricaoEntrada);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataInicialTriagem()
	{
		msgDataInicialTriagem = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[3]/div[1]/div[2]/span");
		return Text(msgDataInicialTriagem);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataFinalTriagem()
	{
		msgDataFinalTriagem = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[3]/div[2]/div[2]/span");
		return Text(msgDataFinalTriagem);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoAtendimento()
	{
		msgAnoAtendimento = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[5]/div/span");
		return Text(msgAnoAtendimento);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeLimiteObras()
	{
		msgLimiteObras = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[7]/div/span");
		return Text(msgLimiteObras);
	}
	
	public static String validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataPublicacaoDou()
	{
		msgDataPublicacaoDou = GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/form/div[8]/div[6]/div/div[2]/span");
		return Text(msgDataPublicacaoDou);
	}
}
