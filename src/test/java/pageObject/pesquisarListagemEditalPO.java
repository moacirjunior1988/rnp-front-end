package pageObject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class pesquisarListagemEditalPO extends seleniumActions
{
	private static WebElement btnCadastrarEdital() { return GetElement(byElements.XPath, "//button[text()='Cadastrar Edital']"); }
	private static WebElement grdCabecalhoListagemEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/table/thead/tr"); }
	private static List<WebElement> grdListagemEdital() { return GetElements(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/table/tbody/tr"); }
	private static WebElement grdAcoesEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/table/tbody/tr[1]/td[6]/div"); }
	public static List<WebElement> lstSVGListagemEdital () { return grdAcoesEdital().findElements(By.xpath("//*[name()='svg']")); }
	private static WebElement pgListagemEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/h1"); }
	private static WebElement txtPesquisar() { return GetElement(byElements.Name, "term"); }
	private static WebElement btnPesquisarEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[1]/div/form/button"); }
	private static WebElement clnTituloEdital() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/table/tbody/tr[1]/td[1]"); }
	private static WebElement clnAnoReferencia() { return GetElement(byElements.XPath, "//*[@id=\"root\"]/div/div[2]/div[2]/table/tbody/tr[1]/td[2]"); }
	//private static WebElement pgDetalhesEdital() { return GetElement(byElements.XPath, "Teste"); }
	
	//Click
	public static void pesquisar(String text)
	{
		Sendkeys(txtPesquisar(), text);
	}
	
	public static void cadastrarEdital()
	{
		Clicks(btnCadastrarEdital());
	}
	
	public static void pesquisarEdital()
	{
		Clicks(btnPesquisarEdital());
	}
	
	public static void visualizarEdital()
	{
		Clicks(lstSVGListagemEdital().get(4));
	}
	
	public static void alterarEdital()
	{
		Clicks(lstSVGListagemEdital().get(5));
	}
	
	public static void cadastrarObjeto()
	{
		Clicks(lstSVGListagemEdital().get(5));
	}
	
	public static void cadastrarObjeto2()
	{
		Clicks(lstSVGListagemEdital().get(6));
	}
	
	public static void alterarObjeto()
	{
		Clicks(lstSVGListagemEdital().get(2));
	}
	
	//Valida��o da exibi��o de tela
	public static boolean validationOfExibitionOfPageListagemEdital()
	{
		return ValidacoesDeElementos(pgListagemEdital());
	}
	
	public static boolean validationOfExibitionOfRegisterNotice(int index)
	{
		return ValidacoesDeElementos(grdListagemEdital().get(index));
	}
	
	// Valida��o da exibi��o das colunas da listagem de editais
	public static  boolean validationOfExibitionOfColunsOfListOfNotices(String nameColuns)
	{
		if(Text(grdCabecalhoListagemEdital()).contains(nameColuns)) return true;
		else return false;
	}
	
	// Valida��o do registro apresentado da pesquisa
	public static boolean validationOfExibitionOfRegisterOfQuery(String text)
	{
		if(Text(grdListagemEdital().get(0)).contains(text)) return true;
		else return false;
	}
	
	//Retorno dos textos das colunas da listagem de edital
	public static String retornoDosTextosDasColunasDaListagemDeEditalColunaTitulo()
	{
		return Text(clnTituloEdital());
	}
	
	public static String retornoDosTextosDasColunasDaListagemDeEditalColunaAnoReferencia()
	{
		return Text(clnAnoReferencia());
	}
	
	// Valida��o da exibi��o da tela detalhes do edital
	public static boolean  validationOfExibitionOfPageDetailOfNotice()
	{
		return true; //ValidacoesDeElementos(pgDetalhesEdital());
	}
}
