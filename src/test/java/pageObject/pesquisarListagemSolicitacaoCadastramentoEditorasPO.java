package pageObject;

import java.util.List;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class pesquisarListagemSolicitacaoCadastramentoEditorasPO extends seleniumActions
{
	//Elements
	private static WebElement pgListagemSolicitacao() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement grdCabecalhoListagemSolicitacao() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement cbxFiltros() { return GetElement(byElements.XPath, "Teste"); }
	private static List<WebElement> grdLinhasListagemSolicitacao() { return GetElements(byElements.XPath, "Teste"); }
	private static WebElement txtPesquisar() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement btnPesquisarListagemSolicitacao() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement clnCNPJ() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement clnRazaoSocial() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement btnAprovarReprovar() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement mdlSolicitacao() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement rbtAprovar() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement rbtReprovar() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement btnConfirmarSolicitacao() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement txtJustificativa() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement btnHistorico() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement grdCabecalhoListagemHistoricoSolicitacaoEditora() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement btnJustificativa() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement mdlJustificativa() { return GetElement(byElements.XPath, "Teste"); }
	private static WebElement lblTextoJustificativa() { return GetElement(byElements.XPath, "Teste"); }
	
	//Sendkeys
	public static void pesquisar(String text)
	{
		Sendkeys(txtPesquisar(), text);
	}
	
	public static void justificativa(String text)
	{
		Sendkeys(txtJustificativa(), text);
	} 
	
	//Combobox
	public static void filtros(String text)
	{
		ComboBoxSelect(cbxFiltros(), text);
	}
	
	//Click
	public static void pesquisarSolicitacao()
	{
		Clicks(btnPesquisarListagemSolicitacao());
	}
	
	public static void aprovarReprovar()
	{
		Clicks(btnAprovarReprovar());
	}
	
	public static void confirmarSolicitacao()
	{
		Clicks(btnConfirmarSolicitacao());
	}
	
	public static void historico()
	{
		Clicks(btnHistorico());
	}
	
	public static void justificativa()
	{
		Clicks(btnJustificativa());
	}
	
	//RadioButton
	public static void aprovar()
	{
		Clicks(rbtAprovar());
	}
	
	public static void reprovar()
	{
		Clicks(rbtReprovar());
	}
	
	// Valida��o da exibi��o da tela Listagem Solicita��o
	public static boolean  validationOfExibitionOfPageListagemSolicitacao()
	{
		return ValidacoesDeElementos(pgListagemSolicitacao());
	}
	
	//Valida��o da exibi��o da Modal de Aprovar ou Rejeitar Solicita��o
	public static boolean  validationOfExibitionOfPageModalSolicitacao()
	{
		return ValidacoesDeElementos(mdlSolicitacao());
	}
	
	//Valida��o da exibi��o da Modal de Aprovar ou Rejeitar Solicita��o
	public static boolean  validationOfExibitionOfPageModalJustificativaDaSolciitacao()
	{
		return ValidacoesDeElementos(mdlJustificativa());
	}
	
	// Valida��o da exibi��o da Listagem do Hist�rico da Solicita��o
	public static boolean  validationOfExibitionOfGridListagemHistoricoSolicitacao()
	{
		return ValidacoesDeElementos(grdCabecalhoListagemHistoricoSolicitacaoEditora());
	}
	
	// Valida��o da exibi��o das colunas da listagem de solicita��es de cadastramento de editora
	public static  boolean validationOfExibitionOfColunsOfListOfSolicitacoes(String nameColuns)
	{
		if(Text(grdCabecalhoListagemSolicitacao()).contains(nameColuns)) return true;
		else return false;
	}
	
	// Valida��o da exibi��o das colunas da listagem de historico da solicita��o do cadastramento da editora
	public static  boolean validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(String nameColuns)
	{
		if(Text(grdCabecalhoListagemHistoricoSolicitacaoEditora()).contains(nameColuns)) return true;
		else return false;
	}
	
	// Valida��o de registro filtro todos
	public static boolean validationOfExibitionOfRegisterListagemSolicitacao(int index)
	{
		return ValidacoesDeElementos(grdLinhasListagemSolicitacao().get(index));
	}
	
	//Retorno dos textos das colunas da listagem de edital
	public static String retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaCNPJ()
	{
		return Text(clnCNPJ());
	}
	
	public static String retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaRazaoSocial()
	{
		return Text(clnRazaoSocial());
	}
	
	// Retorno do texto da justificativa da reprova��o da solicita��o
	public static String retornoDosTextosDaJustificativaDaResprovacaoDaSolicitacao()
	{
		return Text(lblTextoJustificativa());
	}
}
