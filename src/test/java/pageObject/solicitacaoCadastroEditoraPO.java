package pageObject;

import org.openqa.selenium.WebElement;

import enums.byElements;
import geral.seleniumActions;

public class solicitacaoCadastroEditoraPO extends seleniumActions
{
	//Elements
	private static WebElement btnSolicitacao() { return GetElement(byElements.Id, "teste"); }
	private static WebElement pgSolicitacao() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtCNPJ() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtRazaoSocial() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtNomeFantasia() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtCPFResponsavel() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtNomeResponsavel() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtTelefoneResponsavel() { return GetElement(byElements.Id, "teste"); }
	private static WebElement txtEmailResponsavel() { return GetElement(byElements.Id, "teste"); }
	private static WebElement cbxVincularEditora() { return GetElement(byElements.Id, "teste"); }
	private static WebElement btnCancelar() { return GetElement(byElements.Id, "teste"); }
	private static WebElement btnSolicitar() { return GetElement(byElements.Id, "teste"); }
	
	
	//Sendkeys
	public static void cnpj(String text)
	{
		if(text.length() > 0) Sendkeys(txtCNPJ(), text);
		else Clear(txtCNPJ());
	}
	
	public static void razaoSocial(String text)
	{
		if(text.length() > 0) Sendkeys(txtRazaoSocial(), text);
		else Clear(txtRazaoSocial());
	}
	
	public static void nomeFantasia(String text)
	{
		if(text.length() > 0) Sendkeys(txtNomeFantasia(), text);
		else Clear(txtNomeFantasia());
	}
	
	public static void cpfResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtCPFResponsavel(), text);
		else Clear(txtCPFResponsavel());
	}
	
	public static void nomeResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtNomeResponsavel(), text);
		else Clear(txtNomeResponsavel());
	}
	
	public static void telefoneResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtTelefoneResponsavel(), text);
		else Clear(txtTelefoneResponsavel());
	}
	
	public static void emailResponsavel(String text)
	{
		if(text.length() > 0) Sendkeys(txtEmailResponsavel(), text);
		else Clear(txtEmailResponsavel());
	}
	
	//Combobox
	public static void vincularAoEdital(String text)
	{
		ComboBoxSelect(cbxVincularEditora(), text);
	}
	
	//Click
	public static void Solicitacao()
	{
		Clicks(btnSolicitacao());
	}
	
	public static void cancelar()
	{
		Clicks(btnCancelar());
	}
		
	public static void solicitar()
	{
		Clicks(btnSolicitar());
	}
	
	//Valida��o da exibi��o de tela
	public static boolean validationOfExibitionOfPageSolicitacao()
	{
		return ValidacoesDeElementos(pgSolicitacao());
	}
}
