package steps;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.java.pt.Dado;
import geral.extentReport;
import geral.seleniumActions;
import geral.testConfiguration;
import io.github.bonigarcia.wdm.WebDriverManager;

public class acessarSistemaSteps 
{
	protected ExtentTest extentTest;
	private extentReport extentReport = new extentReport();
    
	public acessarSistemaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
	
	@Dado("que acesso a tela de login do sistema pnld digital$")
    public void dadoQueAcessoATelaDeLoginDoSistema()
    {
    	extentReport.SalvarLogSucesso("Dado que acesso a tela de login do sistema pnld digital.");
       
    	WebDriverManager.chromedriver().setup();
    	ChromeOptions options = new ChromeOptions();
    	options.setHeadless(false);
    	seleniumActions.webDriver = new ChromeDriver();
    	seleniumActions.wait = new WebDriverWait(seleniumActions.webDriver, 200);
    	seleniumActions.webDriver.navigate().to(testConfiguration.url);
    	seleniumActions.webDriver.manage().window().maximize();
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("tela de login exibida.");
    }
}
