package steps;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.java.pt.E;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;

public class alterarCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    
    public alterarCategoriaSteps()
    {
         this.extentTest = extentReport.test;
    }
	
	@E("clico no bot�o editar categoria$")
	public void eClicoNoBotaoEditarCategoria()
	{
		extentReport.SalvarLogScreenShotSucesso("E clico no botao editar categoria.");
	}
}
