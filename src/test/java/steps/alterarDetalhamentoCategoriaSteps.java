package steps;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.java.pt.E;
import geral.testConfiguration;
import geral.utils;

public class alterarDetalhamentoCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    
    public alterarDetalhamentoCategoriaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico na acao editar de um detalhamento da categoria$")
    public void eClicoNaAcaoEditarDeUmDetalhamentoDaCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico na a��o editar de um detalhamento da categoria.");
    }
    
    @E("clico no botao alterar da tela de alteracao do detalhamento da categoria$")
    public void eNoBotaoAlterarDaTelaDeAlteracaoDoDatalhamentoDaCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no bot�o alterar da tela de altera��o do detalhamento da categoria.");
    }
}
