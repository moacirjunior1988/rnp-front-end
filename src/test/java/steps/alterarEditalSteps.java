package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.testConfiguration;
import geral.utils;
import pageObject.*;

public class alterarEditalSteps
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    Random random = new Random();
    editalPO editalPO = new editalPO();
    private String typoEdital = "PNLD EJA,PNLD CAMPO,PNLD LITERARIO,PNLD CONCURSO,PNLD PEDAGOGICO,PNLD";
    private String[] listaTipoEdital = typoEdital.split(",");
    
    public alterarEditalSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico no botao alterar de um edital ja cadastrado$")
    public void eClicoNoBotaoAlterarDeUmEditalJaCadastrado()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao alterar de um edital ja cadastrado.");
    	pesquisarListagemEditalPO.alterarEdital();
    }
    
    @Quando("apresentar a tela de alteracao de edital$")
    public void quandoApresentarATelaDeAlteracaoDeEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a tela de cadastro de edital.");
    	
    	assertTrue(pageObject.editalPO.validationOfExibitionOfPageInsertEdital());
    }
    
    @E("irei alterar um dos campos obrigatorios do edital$")
    public void eIreiAlterarUmDosCamposObrigatoriosDoEdital(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E irei alterar um dos campos obrigatorios do edital.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (Integer.parseInt(data.get(0).get("contador"))) 
    	{
			case 1:
				pageObject.editalPO.tituloEdital("Edital Teste " + utils.geradorNumeroRandomico(3));
			break;
				
			case 2:
				pageObject.editalPO.anoReferencia(geral.utils.anosAnterioresAtual(random.nextInt(9)));
			break;
			
			case 3:	    	
		    	pageObject.editalPO.tipoEdital(listaTipoEdital[random.nextInt((listaTipoEdital.length - 1))]);
			break;
			
			case 4:
				pageObject.editalPO.numeroEdital(Integer.toString(utils.geradorNumeroRandomico(4)));
			break;
		}
    }
    
    @E("clico no botao alterar edital$")
    public void eClicoNoBotaoAlterarEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao alterar edital.");
    	
    	pageObject.editalPO.alterarOEdital();	
    }
    
    @E("irei apagar a informacao que esta em um campo obrigatorio do edital$")
    public void eIreiApagarAInformacaoQueEstaEmUmCampoObrigatorioDoEdital(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E irei apagar a informacao que esta em um campo obrigatorio do edital.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("campo")) 
    	{
			case "tituloEdital":
				pageObject.editalPO.tituloEdital("");
			break;
				
			case "anoReferencia":
				pageObject.editalPO.anoReferencia("");
			break;
			
			case "tipoEdital":
				pageObject.editalPO.tipoEdital("");
			break;
			
			case "numeroEdital":
				pageObject.editalPO.numeroEdital("");
			break;
		}
    }
    
    @E("irei apagar todos os campos obrigatorios da tela de alterar edital$")
    public void eIreiApagattodosOsCamposObrigaoriosDaTelaDeAlterarEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E irei apagar todos os campos obrigatorios da tela de alterar edital.");
    
    	pageObject.editalPO.tituloEdital("");
    	pageObject.editalPO.anoReferencia("");
    	//pageObject.editalPO.tipoEdital("");
    	pageObject.editalPO.numeroEdital("");
    }

    @E("irei informarar dados de um edital ja cadastrado$")
    public void eIreIreiInformarDadosDeUmEditalJaCadastrado(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E irei informarar dados de um edital ja cadastrado.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pageObject.editalPO.tituloEdital(data.get(0).get("tituloEdital"));
    	
    	pageObject.editalPO.anoReferencia(data.get(0).get("anoReferencia"));
    	
    	pageObject.editalPO.tipoEdital(data.get(0).get("tipoEdital"));
    	
    	pageObject.editalPO.numeroEdital(data.get(0).get("numeroEdital"));
    }
}
