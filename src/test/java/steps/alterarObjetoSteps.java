package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import pageObject.homePO;
import pageObject.objetoPO;
import pageObject.pesquisarEditoraPO;
import pageObject.pesquisarListagemEditalPO;

public class alterarObjetoSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    homePO homePO = new homePO();
    objetoPO objetoPO = new objetoPO();
    
    public alterarObjetoSteps()
    {
         this.extentTest = extentReport.test;
    }
    
    @Dado("que estou na tela de listagem de edital com editais e objetos cadastrados$")
    public void dadoQueEstouNaTelaDeListagemDeEditalComEditaisEObjetosCadastrados()
    {
    	extentReport.SalvarLogSucesso("Dado que estou na tela de listagem de edital com editais e objetos cadastrados.");
    	
    	assertTrue(homePO.validationOfExibitionOfPageListagemEdital());
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterNotice(0));
    	
    	extentReport.SalvarLogScreenShotSucesso("tela de listagem de edital exibida com sucesso.");
    }
    
    @E("clico no botao editar de um objeto ja cadastrado em um edital$")
    public void eClicoNoBotaoEditarDeUmObjetoJaCadastradoEmUmEdital()
    {
    	extentReport.SalvarLogSucesso("E clico no botao editar de um objeto ja cadastrado em um edital.");
    	
    	pesquisarListagemEditalPO.alterarObjeto();
    }
    
    @Quando("apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas$")
    public void quandoApresentarATelaDeCadastroDeObjetoComAsInformacoesCarregadasEAsAbasSendoExibidas()
    {
    	extentReport.SalvarLogSucesso("Quando apresentar a tela de cadastro de objeto com as informacoes carregadas e as abas sendo exibidas.");
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaComplemento());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaEditoras());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaEstrutura());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaOrganizacao());
    	
    	assertTrue(objetoPO.validacaoDeCampoPreenchidoDoEditarObjeto());
    }
    
    @E("irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado$")
    public void eIreiAlterarOuApagarUmDosCamposObrigatoriosDoObjetoCadastrado(DataTable dataTable)
    {
    	extentReport.SalvarLogSucesso("E irei alterar ou apagar um dos campos obrigatorios do objeto cadastrado.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("nomeCampoAlterar")) 
    	{
			case "nomeObjeto":
				if(data.get(0).get("nomeObjeto").length() > 0) objetoPO.nomeObjeto(data.get(0).get("nomeObjeto") + " " + utils.geradorNumeroRandomico(3));
				else objetoPO.nomeObjeto(data.get(0).get("nomeObjeto"));
				break;
	
			case "codigoObjeto":
				if(data.get(0).get("codigoObjeto").equals("alterar")) objetoPO.codigoObjeto(Integer.toString(utils.geradorNumeroRandomico(3)));
		    	else objetoPO.codigoObjeto(data.get(0).get("codigoObjeto"));
				break;
				
			case "habilitarCadastroObras":
				objetoPO.habilitarCadastroObras(data.get(0).get("habilitarCadastroObras"));
				break;
				
			case "dataHoraAbertura":
				if(data.get(0).get("dataHoraAbertura").equals("alterar")) objetoPO.dataHoraAbertura(utils.geradorDataPassado(100, 2));
		    	else objetoPO.dataHoraAbertura(data.get(0).get("dataHoraAbertura"));
				break;
				
			case "dataHoraFechamento":
				if(data.get(0).get("dataHoraFechamento").equals("alterar")) objetoPO.dataHorafechamento(utils.geradorDataFutura(100, 2));
		    	else objetoPO.dataHorafechamento(data.get(0).get("dataHoraFechamento"));
				break;
				
			case "habilitarfases":
				objetoPO.habilitarFases(data.get(0).get("habilitarfases"));
		    	break;
		    	
			case "dataInicialInscricaoEntrada":
				if(data.get(0).get("dataInicialInscricaoEntrada").equals("alterar")) objetoPO.dataInicialPeriodoInscricaoEntrada(utils.geradorDataPassado(100, 2));
		    	else objetoPO.dataInicialPeriodoInscricaoEntrada(data.get(0).get("dataInicialInscricaoEntrada"));
				break;
				
			case "dataFinalInscricaoEntrada":
				if(data.get(0).get("dataFinalInscricaoEntrada").equals("alterar")) objetoPO.dataFinalPeriodoInscricaoEntrada(utils.geradorDataFutura(100, 2));
		    	else objetoPO.dataFinalPeriodoInscricaoEntrada(data.get(0).get("dataFinalInscricaoEntrada"));
				break;
				
			case "dataInicialTriagem":
				if(data.get(0).get("dataInicialTriagem").equals("alterar")) objetoPO.dataInicialTriagem(utils.geradorDataPassado(100, 2));
		    	else objetoPO.dataInicialTriagem(data.get(0).get("dataInicialTriagem"));
				break;
				
			case "dataFinalTriagem":
				if(data.get(0).get("dataFinalTriagem").equals("alterar")) objetoPO.dataFinalTriagem(utils.geradorDataFutura(120, 2));
		    	else objetoPO.dataFinalTriagem(data.get(0).get("dataFinalTriagem"));
				break;
				
			case "anoAtendimento":
				if(data.get(0).get("anoAtendimento").equals("alterar")) objetoPO.anoAtendimento(utils.anoPassado());
		    	else objetoPO.anoAtendimento(data.get(0).get("anoAtendimento"));
				break;
				
			case "situacaoObras":
				objetoPO.situacaoObras(data.get(0).get("situacaoObras"));
				break;
				
			case "limiteObras":
				if(data.get(0).get("limiteObras").equals("alterar")) objetoPO.limiteObras(Integer.toString(utils.geradorNumeroRandomico(9)));
		    	else objetoPO.limiteObras(data.get(0).get("limiteObras"));
				break;
				
			case "dataPublicacaoDou":
				if(data.get(0).get("dataPublicacaoDou").equals("alterar")) objetoPO.dataPublicacaoDou(utils.geradorDataPassado(120, 2));
		    	else objetoPO.dataPublicacaoDou(data.get(0).get("dataPublicacaoDou"));
				break;
		}
    }
}
