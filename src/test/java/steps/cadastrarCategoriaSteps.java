package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import pageObject.categoriaPO;
import pageObject.mensagensCategoriaPO;

public class cadastrarCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    int codigoCategoria = 0;
    
    public cadastrarCategoriaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico na aba categoria$")
    public void eClicoNaAbaCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico na aba categoria.");
    	
    	categoriaPO.abaCategoria();
    }
    
    @Quando("apresentar a tela de listagem de categorias$")
    public void quandoApresentarATelaDeListagemDeCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a tela de listagem de categoria.");
    	
    	assertTrue(categoriaPO.validationOfExibitionOfGridCategorias());
    }
    
    @E("clico no botao cadastrar categoria$")
    public void eClicoNoBotaoCadastrarCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no bot�o cadastrar categoria.");
    	
    	categoriaPO.cadastrarCategoria();
    }
    
    @E("informarei corretamente todos os campos obrigatorios do cadastro de categoria$")
    public void eInformareiCorretamenteTodosOsCamposObrigatpriosDoCadastroDeCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei corretamente todos os campos obrigatorios do cadastro de categoria.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	codigoCategoria =  utils.geradorNumeroRandomico(3);
    	
    	if(data.get(0).get("categoria").equals("inserir")) categoriaPO.nome("Categoria Teste " + codigoCategoria);
    	else categoriaPO.nome(data.get(0).get("categoria"));
    	
    	if(data.get(0).get("codigoCategoria").equals("inserir")) categoriaPO.codigo(Integer.toString(codigoCategoria));
    	else categoriaPO.codigo(data.get(0).get("codigoCategoria"));
    }
    
    @E("clico no botao cadastrar da tela de cadastro de categoria$")
    public void eClicoNoBotaoCadastrarDaTelaDeCadastroDeCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao cadastrar da tela de cadastro de categoria.");
    	
    	categoriaPO.cadastrar();
    }
    
    @E("nao informo um ou todos os campos do cadastro de categoria$")
    public void eNaoInformoUmOuTodosOsCamposDoCadastroDeCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E nao informo um ou todos os campos do cadastro de categoria.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	codigoCategoria =  utils.geradorNumeroRandomico(3);
    	
    	if(data.get(0).get("categoria").equals("inserir")) categoriaPO.nome("Categoria Teste " + codigoCategoria);
    	else categoriaPO.nome(data.get(0).get("categoria"));
    	
    	if(data.get(0).get("codigoCategoria").equals("inserir")) categoriaPO.codigo(Integer.toString(codigoCategoria));
    	else categoriaPO.codigo(data.get(0).get("codigoCategoria"));
    }
    
    @Entao("devera apresentar uma mensagem de validacao da tela de categoria$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDaTelaDeCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma mensagem de validacao da tela de categoria.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	assertEquals(data.get(0).get("mensagem"), mensagensCategoriaPO.returnTextMensagemCadastrarAlterarDuplicidadeCategoria());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagem de valida��o exibida corretamente.");
    	hooks.status = Status.PASS;
    }
}
