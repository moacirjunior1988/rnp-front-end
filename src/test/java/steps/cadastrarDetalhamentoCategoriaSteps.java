package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import geral.testConfiguration;
import geral.utils;

public class cadastrarDetalhamentoCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    
    public cadastrarDetalhamentoCategoriaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico na acao detalhamento da categoria$")
    public void eClicoNaAcaoDetalhamentoDaCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico na acao detalhamento da categoria.");
    }
    
    @E("informarei corretamente todos os campos obrigatorios do cadastro de detalhamento de categoria$")
    public void eInformareiCorretamenteTodosOsCamposObrigatoriosDoCadastroDeDetalhamentoDeCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei corretamente todos os campos obrigatorios do cadastro de detalhamento de categoria.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
    
    @E("clico no botao cadastrar da tela de cadastro de detalhamento da categoria$")
    public void eClicoNobotaoCadastrarDaTelaDeCadastroDeDetalhamentoDaCategoria()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao cadastrar da tela de cadastro de detalhamento da categoria.");
    }
    
    @Entao("devera apresentar uma mensagem de validacao da tela de detalhemento da categoria$")
    public void entaoDeverapresentarUmaMensagemDeValidacaoDaTelaDeDetalhamentoDaCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Ent�o devera apresentar uma mensagem de validacao da tela de detalhemento da categoria.");
    }
    
    @E("nao informo um ou todos os campos do cadastro de detalhamento de categoria")
    public void eNaoInformoUmOuTodosOsCamposDoCadastroDeDetalhamentoDeCategoria(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E nao informo um ou todos os campos do cadastro de detalhamento de categoria.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
}
