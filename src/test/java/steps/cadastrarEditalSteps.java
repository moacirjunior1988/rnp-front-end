package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.Random;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.utils;
import pageObject.*;

public class cadastrarEditalSteps 
{
	protected ExtentTest extentTest;
	private utils utils = new utils();
	private Random random = new Random();
	private String typoEdital = "PNLD EJA,PNLD CAMPO,PNLD LITERARIO,PNLD CONCURSO,PNLD PEDAGOGICO,PNLD";
	private String[] listaTipoEdital = typoEdital.split(",");
    
    public cadastrarEditalSteps()
    {
         this.extentTest = extentReport.test;
    }
    
    @Dado("que estou na tela de listagem de edital$")
    public void dadoQueEstounaTelaDeListagemEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Dado que estou na tela de listagem de edital.");
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfPageListagemEdital());
    }
    
    @E("clico no botao cadastrar edital$")
    public void eClicoNoBotaoCadastrarEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao inserir.");
    	
    	pesquisarListagemEditalPO.cadastrarEdital();
    }
    
    @Quando("apresentar a tela de cadastro de edital$")
    public void quandoApresentarATelaDeCadastroDeEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a tela de cadastro de edital.");
    	
    	assertTrue(editalPO.validationOfExibitionOfPageInsertEdital());
    }
    
    @E("informarei corretamente todos os campos obrigatorios do cadastro de edital$")
    public void eInformareiCorretamenteTodosOsCamposObrigatoriosDoCadastroDeEdital(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei corretamente todos os campos obrigatorios do cadastro de edital.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	if(data.get(0).get("tituloEdital").equals("inserir")) editalPO.tituloEdital("Edital Teste " + utils.geradorNumeroRandomico(3));
    	else editalPO.tituloEdital(data.get(0).get("tituloEdital"));
    	
    	if(data.get(0).get("anoReferencia").equals("inserir")) editalPO.anoReferencia(geral.utils.anoAtual());
    	else editalPO.anoReferencia(data.get(0).get("anoReferencia"));
    	
    	if(data.get(0).get("tipoEdital").equals("inserir")) editalPO.tipoEdital(listaTipoEdital[random.nextInt((listaTipoEdital.length - 1))]);
    	else editalPO.tipoEdital(data.get(0).get("tipoEdital"));
    	
    	if(data.get(0).get("numeroEdital").equals("inserir")) editalPO.numeroEdital(Integer.toString(utils.geradorNumeroRandomico(4)));
    	else editalPO.numeroEdital(data.get(0).get("numeroEdital"));
    }
    
    @E("clico no botao cadastrar o edital$")
    public void eClicoNobotaoCadastrarOEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao cadastar.");
    	
    	editalPO.cadastrarOEdital();
    }
    
    @E("nao informo um ou todos os campos do cadastro de edital$")
    public void eNaoInformoUmOuTodosOsCampos(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E nao informo um ou todos os campos do cadastro de edital.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	if(data.get(0).get("tituloEdital").equals("inserir")) editalPO.tituloEdital("Edital Teste " + utils.geradorNumeroRandomico(3));
    	else editalPO.tituloEdital(data.get(0).get("tituloEdital"));
    	
    	if(data.get(0).get("anoReferencia").equals("inserir")) editalPO.anoReferencia(geral.utils.anoAtual());
    	else editalPO.anoReferencia(data.get(0).get("anoReferencia"));
    	
    	if(!data.get(0).get("tipoEdital").equals("")) editalPO.tipoEdital(data.get(0).get("tipoEdital"));
    	else editalPO.tipoEdital(data.get(0).get("tipoEdital"));
    	
    	if(data.get(0).get("numeroEdital").equals("inserir")) editalPO.numeroEdital(Integer.toString(utils.geradorNumeroRandomico(4)));
    	else editalPO.numeroEdital(data.get(0).get("numeroEdital"));
    }
    
    @Entao("devera apresentar uma mensagem de validacao de cadastrar e alterar edital$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDeCadastrEdital(DataTable dataTable)
    {
    	extentReport.SalvarLogScreenShotSucesso("Ent�o devera apresentar uma mensagem de validacao de cadastrar e alterar edital");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	if(data.get(0).get("acao").equals("inserir")) assertEquals(data.get(0).get("mensagem"), mensagensEditalPO.returnTextMensagemCadastrarEdital());
    	else if(data.get(0).get("acao").equals("alterar")) assertEquals(data.get(0).get("mensagem"), mensagensEditalPO.returnTextMensagemAlterarEdital());
    	else if(data.get(0).get("acao").equals("duplicado")) assertEquals(data.get(0).get("mensagem"), mensagensEditalPO.returnTextMensagemDuplicadoEdital());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagem de valida��o exibida corretamente.");
    	hooks.status = Status.PASS;
    }
    
    @Entao("devera apresentar uma mensagem de validacao dos campos obrigatorios do edital$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDosCamposObrigatoriosDoEdital(DataTable dataTable)
    {
    	extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do edital");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (hooks.cenario) 
    	{
			case "Validar campo obrigatorio titulo do edital do cadastro de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTituloEdital(), data.get(0).get("mensagem"));
				break;
	
			case "Validar campo obrigatorio ano de refer�ncia do cadastro de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoReferencia(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campos obrigatorio tipo do edital do cadastro de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTipoEdital(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campos obrigatorio n�mero do edital do cadastro de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNumeroEdital(), data.get(0).get("mensagem"));
				break;
				
			case "Validar todos os campos obrigatorio do cadastro de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTituloEdital(), data.get(0).get("mensagem_1"));
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoReferencia(), data.get(0).get("mensagem_2"));
				//assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTipoEdital(), data.get(0).get("mensagem_3"));
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNumeroEdital(), data.get(0).get("mensagem_4"));
				break;
				
			case "Validar campos obrigatorio titulo do edital da alteracao de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTituloEdital(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campos obrigatorio ano de refer�ncia da alteracao de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoReferencia(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campos obrigatorio tipo do edital da alteracao de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTipoEdital(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campos obrigatorio n�mero do edital da alteracao de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNumeroEdital(), data.get(0).get("mensagem"));
				break;
				
			case "Validar todos os campos obrigatorio da alteracao de edital":
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTituloEdital(), data.get(0).get("mensagem_1"));
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoReferencia(), data.get(0).get("mensagem_2"));
				//assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeTipoEdital(), data.get(0).get("mensagem_3"));
				assertEquals(editalPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNumeroEdital(), data.get(0).get("mensagem_4"));
				break;
		}
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagens de validacao exibidas corretamente.");
    	hooks.status = Status.PASS;
    	if((hooks.cenario.equals("Validar regra de duplicidade de edital ao cadastrar um novo edital")) ||
    	    (hooks.cenario.equals("Validar campo obrigatorio data de publicacao no dou da alteracao do objeto")))
    		hooks.ultimoScenario = true;
    }
    
    @E("cadastro um edital$")
    public void eCadastroUmEdital(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E cadastro um edital.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pesquisarListagemEditalPO.cadastrarEdital();
    	
    	stepBase.tituloEdital = "Edital Teste " + utils.geradorNumeroRandomico(3);
    	
    	if(data.get(0).get("tituloEdital").equals("inserir")) editalPO.tituloEdital(stepBase.tituloEdital);
    	else editalPO.tituloEdital(data.get(0).get("tituloEdital"));
    	
    	if(data.get(0).get("anoReferencia").equals("inserir")) editalPO.anoReferencia(geral.utils.anoAtual());
    	else editalPO.anoReferencia(data.get(0).get("anoReferencia"));
    	
    	if(data.get(0).get("tipoEdital").equals("inserir")) editalPO.tipoEdital(listaTipoEdital[random.nextInt((listaTipoEdital.length - 1))]);
    	else editalPO.tipoEdital(data.get(0).get("tipoEdital"));
    	
    	if(data.get(0).get("numeroEdital").equals("inserir")) editalPO.numeroEdital(Integer.toString(utils.geradorNumeroRandomico(4)));
    	else editalPO.numeroEdital(data.get(0).get("numeroEdital"));
    	
    	editalPO.cadastrarOEdital();
    }
}
