package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import pageObject.editoraPO;
import pageObject.objetoPO;

public class cadastrarEditoraSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    objetoPO objetoPO = new objetoPO();
    editoraPO editoraPO = new editoraPO();

    public cadastrarEditoraSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico na aba editoras$")
    public void eClicoNaAbaEditoras()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico na aba editoras.");
    	
    	pageObject.objetoPO.abaEditora();
    }
    
    @Quando("apresentar a tela de listagem de editoras ja vinculadas ao objeto$")
    public void quandoApresentarATelaDeListagemDeEditorasJaVinculadasAoObjeto()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a tela de listagem de editoras ja vinculadas ao objeto.");
    
    	assertTrue(pageObject.editoraPO.validacaoDeExibicaoDaTelaDeEditoras());
    }
    
    @E("clico no botao cadastrar editora$")
    public void eClicoNoBotaoCadastrarEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao cadastrar editora.");
    	
    	pageObject.editoraPO.cadastrarEditora();
    }
    
    @E("informarei corretamente todos os campos obrigatorios do cadastro de editora$")
    public void eInformareiCorretamenteTodosOsCamposObrigatoriosDoCadastroDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei corretamente todos os campos obrigatorios do cadastro de editora.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	String nomeResponsavel = "Nome Responsavel " + utils.geradorNumeroRandomico(4),
    			razaoSocial = "Editora Teste "+ utils.geradorNumeroRandomico(4);
    	
    	if(data.get(0).get("cnpj").equals("inserir")) pageObject.editoraPO.cnpj(utils.createCNPJ());
    	else pageObject.editoraPO.cnpj(data.get(0).get("cnpj"));
    	
    	if(data.get(0).get("razaoSocial").equals("inserir")) pageObject.editoraPO.razaoSocial(razaoSocial);
    	else pageObject.editoraPO.razaoSocial(data.get(0).get("razaoSocial"));
    	
    	if(data.get(0).get("nomeFantasia").equals("inserir")) pageObject.editoraPO.nomeFantasia(razaoSocial);
    	else pageObject.editoraPO.nomeFantasia(data.get(0).get("nomeFantasia"));
    	
    	if(data.get(0).get("cpfResponsavel").equals("inserir")) pageObject.editoraPO.cpfResponsavel(utils.GeradorCpf());
    	else pageObject.editoraPO.cpfResponsavel(data.get(0).get("cpfResponsavel"));
    	
    	if(data.get(0).get("nomeResponsavel").equals("inserir")) pageObject.editoraPO.nomeResponsavel(nomeResponsavel);
    	else pageObject.editoraPO.nomeResponsavel(data.get(0).get("nomeResponsavel"));
    	
    	if(data.get(0).get("telefoneResponsavel").equals("inserir")) pageObject.editoraPO.telefoneResponsavel(utils.createPhoneNumber());
    	else pageObject.editoraPO.telefoneResponsavel(data.get(0).get("telefoneResponsavel"));

    	if(data.get(0).get("emailResponsavel").equals("inserir")) pageObject.editoraPO.emailResponsavel(utils.createEmailMailinator(nomeResponsavel));
    	else pageObject.editoraPO.emailResponsavel(data.get(0).get("emailResponsavel"));
    }
    
    @E("clico no botao cadastrar da tela de cadastro de editora$")
    public void eClicoNoBotaoCadastrarDatTelaDeCadastroDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao cadastrar da tela de cadastro de editora.");
    
    	pageObject.editoraPO.cadastrarAEditora();
    }
    
    @E("nao informo um ou todos os campos do cadastro de editora$")
    public void eNaoInformoUmOuTodosOsCamposDoCadastroDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E nao informo um ou todos os campos do cadastro de editora.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	String nomeResponsavel = "Nome Responsavel " + utils.geradorNumeroRandomico(4),
    			razaoSocial = "Editora Teste "+ utils.geradorNumeroRandomico(4);
    	
    	if(data.get(0).get("cnpj").equals("inserir")) pageObject.editoraPO.cnpj(utils.createCNPJ());
    	else pageObject.editoraPO.cnpj(data.get(0).get("cnpj"));
    	
    	if(data.get(0).get("razaoSocial").equals("inserir")) pageObject.editoraPO.razaoSocial(razaoSocial);
    	else pageObject.editoraPO.razaoSocial(data.get(0).get("razaoSocial"));
    	
    	if(data.get(0).get("nomeFantasia").equals("inserir")) pageObject.editoraPO.nomeFantasia(razaoSocial);
    	else pageObject.editoraPO.nomeFantasia(data.get(0).get("nomeFantasia"));
    	
    	if(data.get(0).get("cpfResponsavel").equals("inserir")) pageObject.editoraPO.cpfResponsavel(utils.GeradorCpf());
    	else pageObject.editoraPO.cpfResponsavel(data.get(0).get("cpfResponsavel"));
    	
    	if(data.get(0).get("nomeResponsavel").equals("inserir")) pageObject.editoraPO.nomeResponsavel(nomeResponsavel);
    	else pageObject.editoraPO.nomeResponsavel(data.get(0).get("nomeResponsavel"));
    	
    	if(data.get(0).get("telefoneResponsavel").equals("inserir")) pageObject.editoraPO.telefoneResponsavel(utils.createPhoneNumber());
    	else pageObject.editoraPO.telefoneResponsavel(data.get(0).get("telefoneResponsavel"));

    	if(data.get(0).get("emailResponsavel").equals("inserir")) pageObject.editoraPO.emailResponsavel(utils.createEmailMailinator(nomeResponsavel));
    	else pageObject.editoraPO.emailResponsavel(data.get(0).get("emailResponsavel"));
    }
}
