package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.List;
import java.util.Map;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.seleniumActions;
import geral.testConfiguration;
import geral.utils;
import pageObject.*;

public class cadastrarObjetoSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    int codigoObjeto = 0;
    
    public cadastrarObjetoSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Dado("que estou na tela de listagem de edital com editais ja cadastrados$")
    public void dadoQueEstounaTelaDeListagemEditalComEditaisJaCadastrados()
    {
    	extentReport.SalvarLogSucesso("Dado que estou na tela de listagem de edital com editais ja cadastrados.");
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfPageListagemEdital());
    	
    	seleniumActions.Scrool(0, 500);
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterNotice(0));
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("tela de listagem de edital exibida com sucesso.");
    }
    
    @E("clico no botao inserir objeto$")
    public void eClicoNoBotaoInserir()
    {
    	extentReport.SalvarLogSucesso("E clico no botao inserir objeto.");
    	
    	if(hooks.cenario.equals("Validar cadastro de objeto com informacoes validas")) 
    		pesquisarListagemEditalPO.cadastrarObjeto();
    	else
    		pesquisarListagemEditalPO.cadastrarObjeto2();
    }
    
    @Quando("apresentar a tela de cadastro de objeto$")
    public void quandoApresentarATelaDeCadastroDeEdital()
    {
    	extentReport.SalvarLogSucesso("Quando apresentar a tela de cadastro de objeto.");
    	
    	seleniumActions.Scrool(0, 300);
    	
    	assertTrue(objetoPO.validationOfExibitionOfPageCadastrarObjeto());
    }
    
    @E("informarei corretamente todos os campos obrigatorios do cadastro de objeto$")
    public void eInformareiCorretamenteTodosOsCamposObrigatoriosDoCadastroDeObjeto(DataTable dataTable)
    {
    	extentReport.SalvarLogSucesso("E informarei corretamente todos os campos obrigatorios do cadastro de objeto.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	codigoObjeto = utils.geradorNumeroRandomico(3);
    	
    	if(data.get(0).get("nomeObjeto").equals("inserir")) objetoPO.nomeObjeto("Objeto " + codigoObjeto);
    	else objetoPO.nomeObjeto(data.get(0).get("nomeObjeto"));
    	
    	if(data.get(0).get("codigoObjeto").equals("inserir")) objetoPO.codigoObjeto(Integer.toString(codigoObjeto));
    	else objetoPO.codigoObjeto(data.get(0).get("codigoObjeto"));
    	
    	pageObject.objetoPO.habilitarCadastroObras(data.get(0).get("habilitarCadastroObras"));
    	
    	seleniumActions.Scrool(0, 500);
    	
    	if(data.get(0).get("dataHoraAbertura").equals("inserir")) objetoPO.dataHoraAbertura(utils.geradorDataHoraPassado(120, 2));
    	else objetoPO.dataHoraAbertura(data.get(0).get("dataHoraAbertura"));
    	
    	if(data.get(0).get("dataHoraFechamento").equals("inserir")) objetoPO.dataHorafechamento(utils.geradorDataHoraFutura(120, 2));
    	else objetoPO.dataHorafechamento(data.get(0).get("dataHoraFechamento"));
    	
    	seleniumActions.Scrool(0, 700);
    	
    	pageObject.objetoPO.habilitarFases(data.get(0).get("habilitarfases"));
    	
    	seleniumActions.Scrool(0, 950);
    	
    	if(data.get(0).get("dataInicialPeriodoInscricaoEntrada").equals("inserir")) objetoPO.dataInicialPeriodoInscricaoEntrada(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataInicialPeriodoInscricaoEntrada(data.get(0).get("dataInicialPeriodoInscricaoEntrada"));
    	
    	if(data.get(0).get("dataFinalPeriodoInscricaoEntrada").equals("inserir")) objetoPO.dataFinalPeriodoInscricaoEntrada(utils.geradorDataFutura(120, 2));
    	else objetoPO.dataFinalPeriodoInscricaoEntrada(data.get(0).get("dataFinalPeriodoInscricaoEntrada"));
    	
    	if(data.get(0).get("dataInicalPeriodoTriagem").equals("inserir")) objetoPO.dataInicialTriagem(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataInicialTriagem(data.get(0).get("dataInicalPeriodoTriagem"));
    	
    	if(data.get(0).get("dataFinalPeriodoTriagem").equals("inserir")) objetoPO.dataFinalTriagem(utils.geradorDataFutura(120, 2));
    	else objetoPO.dataFinalTriagem(data.get(0).get("dataFinalPeriodoTriagem"));
    	
    	if(data.get(0).get("anoAtendimento").equals("inserir")) objetoPO.anoAtendimento(geral.utils.anoAtual());
    	else objetoPO.anoAtendimento(data.get(0).get("anoAtendimento"));
    	
    	if(data.get(0).get("dataPublicacaoDou").equals("inserir")) objetoPO.dataPublicacaoDou(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataPublicacaoDou(data.get(0).get("dataPublicacaoDou"));
    	
    	if(data.get(0).get("limiteObras").equals("inserir")) objetoPO.limiteObras(Integer.toString(utils.geradorNumeroRandomico(1)));
    	else objetoPO.limiteObras(data.get(0).get("limiteObras"));
    	
    	seleniumActions.Scrool(0, 1150);
    	
    	pageObject.objetoPO.situacaoObras(data.get(0).get("situacaoObras"));
    }
    
    @E("clico no botao salvar$")
    public void eClicoNoBotaoSalvar()
    {
    	extentReport.SalvarLogSucesso("E clico no botao gravar.");
    	
    	objetoPO.salvar();
    }
    
    @Entao("devera apresentar na mesma tela de cadastro abas dados, complemento, editoras, estrutura e organizacao$")
    public void entaoDeveraApresentarNaMesmaTelaDeCadastroAbasDadosComplementoEditorasEstruturaEOrganização()
    {
    	extentReport.SalvarLogSucesso("Entao devera apresentar na mesma tela de cadastro abas dados, complemento, editoras, estrutura e organizacao.");
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaComplemento());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaEditoras());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaEstrutura());
    	
    	assertTrue(objetoPO.validationOfExibitionOfAbaOrganizacao());
    	
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao da exibicao das abas realisada com sucesso.");
    }
    
    @E("nao informo um ou todos os campos do cadastro de objeto$")
    public void eNaoInformoUmOuTodosOsCamposDoCadastroDeObjeto(DataTable dataTable)
    {
    	extentReport.SalvarLogSucesso("E nao informo um ou todos os campos do cadastro de objeto.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	codigoObjeto = utils.geradorNumeroRandomico(3);
    	
    	if(data.get(0).get("nomeObjeto").equals("inserir")) objetoPO.nomeObjeto("Objeto " + codigoObjeto);
    	else objetoPO.nomeObjeto(data.get(0).get("nomeObjeto"));
    	
    	if(data.get(0).get("codigoObjeto").equals("inserir")) objetoPO.codigoObjeto(Integer.toString(codigoObjeto));
    	else objetoPO.codigoObjeto(data.get(0).get("codigoObjeto"));
    	
    	pageObject.objetoPO.habilitarCadastroObras(data.get(0).get("habilitarCadastroObras"));
    	
    	seleniumActions.Scrool(0, 500);
    	
    	if(data.get(0).get("dataHoraAbertura").equals("inserir")) objetoPO.dataHoraAbertura(utils.geradorDataHoraPassado(120, 2));
    	else objetoPO.dataHoraAbertura(data.get(0).get("dataHoraAbertura"));
    	
    	if(data.get(0).get("dataHoraFechamento").equals("inserir")) objetoPO.dataHorafechamento(utils.geradorDataHoraFutura(120, 2));
    	else objetoPO.dataHorafechamento(data.get(0).get("dataHoraFechamento"));
    	
    	seleniumActions.Scrool(0, 700);
    	
    	pageObject.objetoPO.habilitarFases(data.get(0).get("habilitarfases"));
    	
    	seleniumActions.Scrool(0, 950);
    	
    	if(data.get(0).get("dataInicialPeriodoInscricaoEntrada").equals("inserir")) objetoPO.dataInicialPeriodoInscricaoEntrada(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataInicialPeriodoInscricaoEntrada(data.get(0).get("dataInicialPeriodoInscricaoEntrada"));
    	
    	if(data.get(0).get("dataFinalPeriodoInscricaoEntrada").equals("inserir")) objetoPO.dataFinalPeriodoInscricaoEntrada(utils.geradorDataFutura(120, 2));
    	else objetoPO.dataFinalPeriodoInscricaoEntrada(data.get(0).get("dataFinalPeriodoInscricaoEntrada"));
    	
    	if(data.get(0).get("dataInicalPeriodoTriagem").equals("inserir")) objetoPO.dataInicialTriagem(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataInicialTriagem(data.get(0).get("dataInicalPeriodoTriagem"));
    	
    	if(data.get(0).get("dataFinalPeriodoTriagem").equals("inserir")) objetoPO.dataFinalTriagem(utils.geradorDataFutura(120, 2));
    	else objetoPO.dataFinalTriagem(data.get(0).get("dataFinalPeriodoTriagem"));
    	
    	if(data.get(0).get("anoAtendimento").equals("inserir")) objetoPO.anoAtendimento(geral.utils.anoAtual());
    	else objetoPO.anoAtendimento(data.get(0).get("anoAtendimento"));
    	
    	if(data.get(0).get("dataPublicacaoDou").equals("inserir")) objetoPO.dataPublicacaoDou(utils.geradorDataPassado(120, 2));
    	else objetoPO.dataPublicacaoDou(data.get(0).get("dataPublicacaoDou"));
    	
    	if(data.get(0).get("limiteObras").equals("inserir")) objetoPO.limiteObras(Integer.toString(utils.geradorNumeroRandomico(1)));
    	else objetoPO.limiteObras(data.get(0).get("limiteObras"));
    	
    	seleniumActions.Scrool(0, 1150);
    	
    	pageObject.objetoPO.situacaoObras(data.get(0).get("situacaoObras"));
    }
    
    @Entao("devera apresentar uma mensagem de validacao de cadastro e alteracao do objeto$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDeCadastreAlteracaoDoObjeto()
    {
    	extentReport.SalvarLogSucesso("Entao devera apresentar uma mensagem de validacao de cadastro e alteracao do objeto.");
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfPageListagemEdital());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagem de validacao exibida corretamente.");
    	hooks.status = Status.PASS;
    }
    
    @Entao("devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDosCamposObrigatoriosDoCadastroEAlteracaoDoObjeto(DataTable dataTable)
    {
    	extentReport.SalvarLogSucesso("Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do cadastro e alteracao do objeto.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (hooks.cenario) 
    	{
			case "Validar campo obrigatorio nome objeto do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNomeObjetivo(), data.get(0).get("mensagem"));
				break;
	
			case "Validar campo obrigatorio codigo do objeto do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeCodigoObjetivo(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio habilitar o cadastro de obras do cadastro de objeto":
				//assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedade, data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data e hora de abertura do cadastro de objeto":
				seleniumActions.Scrool(1900, 700);
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataHoraAbertura(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data e hora de fechamento do cadastro de objeto":
				seleniumActions.Scrool(1900, 700);
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataHoraFechamento(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio habilitar fases do cadastro de objeto":
				//assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNomeObjetivo(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data inicial do periodo de inscricao entrega do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataInicalPeriodoInscricaoEntrada(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data final do periodo de inscricao entrega do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataFinalPeriodoInscricaoEntrada(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data inical periodo de triagem do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataInicialTriagem(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data final periodo de triagem do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataFinalTriagem(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio ano(s) de atendimento do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeAnoAtendimento(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio exibir na habilitação obras na situacao do cadastro de objeto":
				//assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeNomeObjetivo(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio limite de obras do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeLimiteObras(), data.get(0).get("mensagem"));
				break;
				
			case "Validar campo obrigatorio data de publicação no dou do cadastro de objeto":
				assertEquals(objetoPO.validacaoDeExibicaoDeMensagensDeObrigatoriedadeDataPublicacaoDou(), data.get(0).get("mensagem"));
				break;
		}
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagens de validacao exibidas corretamente.");
    	hooks.status = Status.PASS;
    	if((hooks.cenario.equals("Validar campo obrigatorio data de publicacao no dou do cadastro de objeto")) ||
    	    (hooks.cenario.equals("Validar campo obrigatorio data de publicacao no dou da alteracao do objeto")))
    		hooks.ultimoScenario = true;
    }
}
