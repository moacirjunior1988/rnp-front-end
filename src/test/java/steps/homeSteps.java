package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import pageObject.homePO;
import pageObject.loginPO;

public class homeSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    loginPO loginPO = new loginPO();
    homePO homePO = new homePO();
    
    public homeSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Dado("que estou na tela home$")
    public void dadoQueEstouNaTelaHome()
    {
    	extentReport.SalvarLogSucesso("Dado que estou na tela home.");
    	
    	assertTrue(pageObject.loginPO.validationOfExibitionOfPage());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("tela home exibida com sucesso.");
    }
    
    @E("acesso o menu$")
    public void eAcessoOMenu(DataTable dataTable)
    {
    	extentTest.info("E acesso o menu.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("menu")) 
    	{
			case "edital":
				pageObject.homePO.menuEdital();
				break;
		}
    }
    
    @Entao("dever� apresentar tela de listagem de edital$")
    public void entaoDeveraApresentarTelaDeListagemDeEdital()
    {
    	extentTest.info("Ent�o dever� apresentar tela de listagem de edital.");
    	
    	assertTrue(pageObject.homePO.validationOfExibitionOfPageListagemEdital());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Tela de listagem de edital exibida com sucesso.");
    }
    
    @Entao("devera apresentar tela de listagem das solicitacoes de cadastramento das editoras$")
    public void entaoDeveraApresentarTelaDasListagemDeSolicitacaoDeCadastramentoDasEditoras()
    {
    	extentTest.info("Entao devera apresentar tela de listagem de solicitacoes de cadastramento das editoras.");
    	
    	assertTrue(pageObject.homePO.validationOfExibitionOfPageListagemSolicitacaoCadastramentoEditora());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Tela de listagem das solicitacoes de cadastramento das editoras exibida com sucesso.");
    }
}
