package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Entao;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;

public class listagemCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    
    public listagemCategoriaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Entao("devera apresentar uma lista de categorias ja cadastradas e a exibindo das colunas$")
    public void entaoDeveraApresentarUmaListaDeCategoriasJaCadastradasEAExibicaoDasColunas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de categorias ja cadastradas e a exibindo das colunas.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
    
    @Entao("devera apresentar em cada linha de categorias na coluna categoria as acoes$")
    public void entaoDeveraApresentarEmCadaLinhaDeCategoriasNaColunaCategoriaAsAcoes(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar em cada linha de categoria na coluna categoria as acoes.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
    
    @Entao("devera apresentar a tela de listagem de categorias vazia$")
    public void entaoDeveraApresentarATelaDeLitagemDeCategoriasVazia(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar a tela de listagem de categorias vazia.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
}
