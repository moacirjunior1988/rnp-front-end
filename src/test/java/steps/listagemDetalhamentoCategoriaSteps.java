package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Entao;
import geral.extentReport;
import geral.testConfiguration;
import geral.utils;

public class listagemDetalhamentoCategoriaSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    
    public listagemDetalhamentoCategoriaSteps()
    {
         this.extentTest = extentReport.test;
    }
    
    @Entao("devera apresentar a tela de listagem de categorias com o detalhamento vazia$")
    public void entaoDeveraApresentarATelaDeListagemDeCategoriasComODetalhementoVazio(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar a tela de listagem de categorias com o detalhamento vazia.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
    
    @Entao("devera apresentar em cada linha de categoria e datelhamento ja cadastradas as acoes na coluna detalhamento$")
    public void entaoDeveraApresentarEmCadaLinhaDeCategoriaEDetalhamentoJaCadastradasAsAcoesNaColunaDetalhamento(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar em cada linha de categoria e datelhamento ja cadastradas as acoes na coluna detalhamento.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
    
    @Entao("devera apresentar a tela de listagem de categorias o detalhamento de uma categoria cadastrada$")
    public void entaoDeveraApresentarATelaDeListagemDeCategoriasODetalhamentoDeUmaCategoriaCadastrada(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar a tela de listagem de categorias o detalhamento de uma categoria cadastrada.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    }
}
