package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.utils;

public class loginSteps 
{
	protected ExtentTest extentTest;
	private utils utils = new utils();
    extentReport extentReport = new extentReport();
    
    public loginSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Dado("que estou na tela de login do sistema pnld digital$")
    public void dadoQueEstouNaTelaDeLoginDoSistema()
    {
    	extentReport.SalvarLogSucesso("Dado que estou na tela de login do sistema pnld digital.");
    	
    	assertTrue(pageObject.loginPO.validationOfExibitionOfPage());
    }
    
    @Quando("informar um usuario e a senha validos$")
    public void quandoInformarUmUsuarioEASenhaValidos(DataTable dataTable)
    {
    	 extentTest.info("Quando informar um usuario e a senha validos");
         List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
         
         pageObject.loginPO.user(data.get(0).get("usuario"));
		 pageObject.loginPO.password(data.get(0).get("senha"));
		 
		 geral.extentReport.SalvarLogScreenShotSucesso("tela de login com usuario e senha preenchido.");
    }
    
    @Quando("nao informamos o usuario ou a senha$")
    public void quandoNaoInformamosOusuarioOuASenha(DataTable dataTable)
    {
    	extentTest.info("Quando nao informamos o usuario ou a senha");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        
    	pageObject.loginPO.user(data.get(0).get("usuario"));
	    pageObject.loginPO.password(data.get(0).get("senha"));
	    
	    geral.extentReport.SalvarLogScreenShotSucesso("tela de login com usuario ou senha preenchido.");
    }

    @E("clico no botao entrar$")
    public void eClicoNoBotaoEntrar()
    {
    	extentTest.info("E clico no botao entrar");
    	
    	pageObject.loginPO.entrar();
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("tela apos entrar no sistema pnld digital");
    }

    @E("clico no recaptche$")
    public void eClicoNoRecaptche()
    {
        extentTest.info("E clico no recaptche");

        pageObject.loginPO.recaptche();

        utils.Sleep(15000);
    }
    
    @Entao("devera apresentar a tela home do sistema$")
    public void entaoDeveraApresentarATelaHomeDoSistema()
    {
    	extentTest.info("Entao devera apresentar a tela home do sistema");
    	
    	assertTrue(pageObject.loginPO.validationOfExibitionOfPage());
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("tela home exibida com sucesso.");
    }
    
    @Entao("devera apresentar uma mensagem de validacao$")
    public void entaoDeveraApresentarUmaMensagemDeValidacao(DataTable dataTable)
    {
    	extentTest.info("Entao devera apresentar uma mensagem de validacao");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (hooks.cenario)
        {
            case "Validando o campo usu�rio vazio":
                assertEquals(data.get(0).get("mensagem"), pageObject.mensagensLoginPO.returnTextMensagemObrigatoriedadeCPFEmail());
                break;

            case "Validando o campo senha vazio":
                assertEquals(data.get(0).get("mensagem"), pageObject.mensagensLoginPO.returnTextMensagemObrigatoriedadeSenha());
                break;

            default:
                assertEquals(data.get(0).get("mensagem"), pageObject.mensagensLoginPO.returnTextMensagemInvalidesCPFEmailESenha());
                break;
        }

    	geral.extentReport.SalvarLogScreenShotSucesso("Mensagem de validacao exibida corretamente.");
    }

    @Quando("^informar um usuario nao cadastrado e a senha$")
    public void quandoInformarUmUsuarioNaoCadastradoEASenha(DataTable dataTable)
    {
        extentTest.info("Quando informar um usuario nao cadastrado e a senha");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        pageObject.loginPO.user(data.get(0).get("usuario"));
        pageObject.loginPO.password(data.get(0).get("senha"));
    }

    @Quando("^informamos o usuario invalido e a senha$")
    public void quandoInformamosOUsuarioInvalidoEASenha(DataTable dataTable)
    {
        extentTest.info("Quando informamos o usuario invalido e a senha");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        pageObject.loginPO.user(data.get(0).get("usuario"));
        pageObject.loginPO.password(data.get(0).get("senha"));
    }
}
