package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.seleniumActions;
import geral.testConfiguration;
import geral.utils;
import pageObject.pesquisarListagemEditalPO;

public class pesquisarEditalSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
	
    
    public pesquisarEditalSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("houver editais ja cadastrados$")
    public void eHouverEditaisJaCadastrados()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E houver editais ja cadastrados.");
    	
    	seleniumActions.Scrool(0, 300);
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterNotice(0));
    }
    
    @Entao("devera apresentar uma lista de editais exibindo as colunas$")
    public void entaoDeveraApresentarUmaListaDeEditaisExibindoAsColunas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editais exibindo as colunas.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	for (int i = 0; i <= 5; i++)
    	{
    		switch (i) 
    		{
				case 0:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_1")));
					break;
	
				case 1:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_2")));
					break;
					
				case 2:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_3")));
					break;
					
				case 3:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_4")));
					break;
					
				case 4:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_5")));
					break;
					
				case 5:
					assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfColunsOfListOfNotices(data.get(0).get("coluna_6")));
					break;
			}
    	}
    	
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
    
    @Quando("informar no campo pesquisar$")
    public void quandoInformarNoCampoPesquisar(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando informar no campo pesquisar.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pesquisarListagemEditalPO.pesquisar(data.get(0).get("pesquisar"));
    }
    
    @E("clico no botao pesquisar edital$")
    public void eClicoNoBotaoPesquisar()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao pesquisar edital.");
    	
    	pesquisarListagemEditalPO.pesquisarEdital();
    }
    
    @Entao("devera retornar os registros especificos pesquisados$")
    public void entaoDeveraRetornarOsRegitrosEspecificosPesquisados(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar os registros especificos pesquisados.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	if((hooks.cenario.equals("Validar pesquisa de edital pela sigla do edital")) || (hooks.cenario.equals("Validar detalhes do edital pesquisando pelo sigla do edital")))
    	{
    		assertEquals(data.get(0).get("pesquisar").substring(0, 1), pesquisarListagemEditalPO.retornoDosTextosDasColunasDaListagemDeEditalColunaTitulo().substring(0,1));
    		assertEquals(data.get(0).get("pesquisar").substring(1, 3), pesquisarListagemEditalPO.retornoDosTextosDasColunasDaListagemDeEditalColunaAnoReferencia().substring(2,4));
    	}
    	else
    	{
    		assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterOfQuery(data.get(0).get("pesquisar")));
    	}
    	hooks.status = Status.PASS;
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Registro pesquisado exibido corretamente.");
    }
    
    @Entao("devera retornar todos registros$")
    public void entaoDeveraRetornarTodosRegistros()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar todos registros.");
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterNotice(0));
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfRegisterNotice(1));
    	
    	hooks.status = Status.PASS;
    	
    	if(hooks.cenario.equals("Validar pesquisa de edital sem passar filtro")) 
    		hooks.ultimoScenario = true;
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Registro pesquisado exibido corretamente.");
    }
    
    @E("clico no botao visualizar de um edital$")
    public void eClicoNoBotaoVisulizarDeUmEdital()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao visualizar de um edital.");
    	
    	pesquisarListagemEditalPO.visualizarEdital();
    }
    
    @Entao("devera apresentar uma tela com as informações detalhadas do edital visualizado$")
    public void entaoDeveraApresentarUmaTelaComAsInformacoesDetalhadasDpEditalVisualizado()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar os registros especificos pesquisados.");
    	
    	assertTrue(pesquisarListagemEditalPO.validationOfExibitionOfPageDetailOfNotice());
    	
    	hooks.status = Status.PASS;
    	
    	if(hooks.cenario.equals("Validar detalhes do edital pesquisando pelo ano do edital")) 
    		hooks.ultimoScenario = true;
    	
    	geral.extentReport.SalvarLogScreenShotSucesso("Tela de detalhes do edital exibido com sucesso.");
    }
    
    @E("pesquiso o edital cadastrado$")
    public void ePesquisoOEditalCadastrado()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E pesquiso o edital cadastrado.");
    	
    	pesquisarListagemEditalPO.pesquisar(stepBase.tituloEdital);
    	
    	pesquisarListagemEditalPO.pesquisarEdital();
    }
}
