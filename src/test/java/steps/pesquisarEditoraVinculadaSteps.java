package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import geral.extentReport;
import geral.hooks;
import geral.utils;
import pageObject.editoraPO;

public class pesquisarEditoraVinculadaSteps 
{
	protected ExtentTest extentTest;
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    editoraPO editoraPO = new editoraPO();
    
    public pesquisarEditoraVinculadaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Entao("devera apresentar uma lista de editoras vinculadas exibindo as colunas$")
    public void entaoDeveraApresentarUmaListaDeEditorasVinculadasExibindoAsColunas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editoras vinculadas exibindo as colunas.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	assertEquals(pageObject.editoraPO.validacaoDeExibicaoDaColunaDeCNPJNaTelaDePesquisaDeEditorasVinculadas(), data.get(0).get("coluna_1"));
    	
    	assertEquals(pageObject.editoraPO.validacaoDeExibicaoDaColunaDeRazaoSocialNaTelaDePesquisaDeEditorasVinculadas() , data.get(0).get("coluna_2"));
    	
    	assertEquals(pageObject.editoraPO.validacaoDeExibicaoDaColunaDePreenchimentoPercentualNaTelaDePesquisaDeEditorasVinculadas() , data.get(0).get("coluna_3"));
    	
    	assertEquals(pageObject.editoraPO.validacaoDeExibicaoDaColunaDeAcoesNaTelaDePesquisaDeEditorasVinculadas() , data.get(0).get("coluna_4"));
    }
    
    @Entao("devera apresentar uma lista de editoras vinculadas e um campo de pesquisa e um botao de pesquisar$")
    public void entaoDeveraApresentarUmaListaDeEditorasVinculadasEUmCampoDePesquisaEUmBotaoDePesquisar()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editoras vinculadas e um campo de pesquisa e um botao de pesquisar");
    	
    	assertTrue(pageObject.editoraPO.validacaoExibicaoCampoPesquisarEditorasVinculadas());
    	
    	assertTrue(pageObject.editoraPO.validacaoExibicaoBotaoPesquisarEditorasVinculadas());
    }
    
    @E("irei informar no campo pesquisar da tela de editoras vinculadas$")
    public void eIreiInformarNoCampopesquisarDaTelaDeEditorasVinculadas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E irei informar no campo pesquisar da tela de editoras vinculadas");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pageObject.editoraPO.pesquisa(data.get(0).get("pesquisar"));
    }
    
    @E("clico no botao pesquisar da tela de editoras vinculadas$")
    public void eClicoNoBotaoPesquisarDaTelaDeEditorasVinculadas()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao pesquisar da tela de editoras vinculadas");
    	
    	pageObject.editoraPO.pesquisar();
    }
    
    @Entao("devera apresentar uma lista de editoras vinculadas exibindo a coluna acoes$")
    public void entaoDeveraApresentarUmalistaDeEditorasVinculadasExibindoAColunaAcoes(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editoras vinculadas exibindo a coluna acoes");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	assertEquals(data.get(0).get("a��es_1"), pageObject.editoraPO.validacaoDasAcoesDaEditoraComAcessoEditar());
    	
    	assertEquals(data.get(0).get("a��es_2"), pageObject.editoraPO.validacaoDasAcoesDaEditoraComAcessoExcluir());
    	
    	assertEquals(data.get(0).get("a��es_3"), pageObject.editoraPO.validacaoDasAcoesDaEditoraComAcesso());
    	
    	assertEquals(data.get(0).get("a��es_4"), pageObject.editoraPO.validacaoDasAcoesDaEditoraComAcessoReiniciarSenha());
    }
    
    @Entao("devera retornar o registro especifico pesquisado da editora vinculada$")
    public void entaoDeveraRetornarORegistroEspecificoPesquisadoDaEditoraVinculada(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar o registro especifico pesquisado da editora vinculada");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	assertEquals(data.get(0).get("pesquisar"), pageObject.editoraPO.validacaoDeRetornoPesquisaEditorasVinculadas());
    }
    
    @E("clico no botao reiniciar senha da tela de editoras vinculadas$")
    public void eClicoNoBotaoReiniciarSenhaDaTelaDeEditorasVinculadas()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao reiniciar senha da tela de editoras vinculadas");
    	
    	pageObject.editoraPO.reiniciarSenha();
    }
    
    @E("clico no botao confirmar do popup de reiniciar senha$")
    public void eClicoNoBotaoConfirmarDoPopupDeReiniciarSenha()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao confirmar do popup de reiniciar senha");
    	
    	pageObject.editoraPO.confirmarReiniciarSenha();
    }
    
    @E("clico no botao sem acesso/com acesso da tela de editoras vinculadas$")
    public void eClicoNobotaoSemAcesoComAcessoDaTelaDeEditorasVinculadas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao sem acesso/com acesso da tela de editoras vinculadas");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("acao")) 
    	{
			case "comAcesso":
				pageObject.editoraPO.comAcesso();
				break;
	
			case "semAcesso":
				pageObject.editoraPO.semAcesso();
				break;
		}
    }
    
    @E("clico no botao permitir no popup da tela de editoras vinculada$")
    public void eClicoNoBotaoPermitirNoPopupDaTelaDeEditorasVinculadas()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editoras vinculadas e um campo de pesquisa e um botao de pesquisar");
    
    	pageObject.editoraPO.permitirOuNaoPermitir();
    }
}
