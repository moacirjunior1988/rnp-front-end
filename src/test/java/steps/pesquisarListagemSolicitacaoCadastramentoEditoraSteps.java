package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.utils;
import pageObject.pesquisarListagemSolicitacaoCadastramentoEditorasPO;

public class pesquisarListagemSolicitacaoCadastramentoEditoraSteps 
{
	protected ExtentTest extentTest;
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    private String cnpjEditora = "";
    
    public pesquisarListagemSolicitacaoCadastramentoEditoraSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @Dado("que estou na tela de listagem de solicitacoes de cadastramento das editoras$")
    public void dadoQueEstouNatelaDeListagemDeSolicitacaoDeCadastramentoDasEditoras()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Dado que estou na tela de listagem de solicitacoes de cadastramento das editoras.");
    	
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfPageListagemSolicitacao());
    }
    
    @Entao("devera apresentar as colunas na lista de solicitacoes de cadastramento de editoras$")
    public void entaoDeveraApresentarAsColunasNaListaDeSolicitacoesDeCadastramentoDeEditoras(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar as colunas na lista de solicitacoes de cadastramento de editoras.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	for(int a = 0; a <= 4; a++)
    	{
    		switch (a) 
    		{
				case 0:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfSolicitacoes(data.get(0).get("coluna_1")));
					break;
	
				case 1:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfSolicitacoes(data.get(0).get("coluna_2")));
					break;
					
				case 2:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfSolicitacoes(data.get(0).get("coluna_3")));
					break;
					
				case 3:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfSolicitacoes(data.get(0).get("coluna_4")));
					break;
					
				case 4:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfSolicitacoes(data.get(0).get("coluna_5")));
					break;
			}
    		
    		hooks.status = Status.PASS;
        	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    	}
    }
    
    @E("clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras$")
    public void eClicoNoFiltroDeSituacoesDaTelaDeSolicitacaoDeCadastramentoDeEditoras(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no filtro de situacoes da tela de solicitacoes de cadastramento de editoras.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.filtros(data.get(0).get("filtro"));
    }
    
    @Entao("devera apresentar todas as solicitacoes com todas as situacoes$")
    public void entaoDeveraApresentarTodasAsSolicitacoesComTodosAsSituacoes()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar todas as solicitacoes com todas as situacoes.");
    	
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfRegisterListagemSolicitacao(0));
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfRegisterListagemSolicitacao(1));
    
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
    
    @Entao("devera apresentar somente as solicitacoes filtradas pelo filtro selecionado$")
    public void entaoDeveraApresntarSomenteAsSolicitacoesFiltradasPeloFiltroSelecionado()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar somente as solicitacoes filtradas pelo filtro selecionado.");
    	
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfRegisterListagemSolicitacao(0));
    	
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
    
    @E("informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora$")
    public void eInformarNoCampoPesquisarDaTelaDeListagemDeSolicitacoesDeCadastramentoDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informar no campo pesquisar da tela de listagem de solicitacoes de cadastramento de editora.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.pesquisar(data.get(0).get("pesquisar"));
    }
    
    @E("clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora$")
    public void eClicoNoBotaoPesquisarDaTelaDeListagemDeSolicitacoesDeCadastramentoDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao pesquisar da tela de listagem de solicitacoes de cadastramento de editora.");
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.pesquisarSolicitacao();
    }
    
    @Entao("devera retornar os registros especificos pesquisados na tela de listagem de solicitacoes de cadastramento de editora$")
    public void entaoDeveraRetornarOsRegistrosEspecificosPesquisadosNaTelaDeListagemDeSolicitacoesDeCadastramentoDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar os registros especificos pesquisados na tela de listagem de solicitacoes de cadastramento de editora.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (hooks.cenario) 
    	{
			case "Validar pesquisa de solicitacoes de cadastramento de editora por CNPJ":
				assertEquals(data.get(0).get(""), pesquisarListagemSolicitacaoCadastramentoEditorasPO.retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaCNPJ());
				break;
	
			case "Validar pesquisa de solicitacoes de cadastramento de editora por razao social":
				assertEquals(data.get(0).get(""), pesquisarListagemSolicitacaoCadastramentoEditorasPO.retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaRazaoSocial());
				break;
		}
    	
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
    
    @E("clico no botao validar de uma das solicitacoes de cadastramento de editora$")
    public void eClicoNoBotaoValidarDeUmaDasSolicitacoesDeCadastramentoDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao validar de uma das solicitacoes de cadastramento de editora.");
    	
    	cnpjEditora = pesquisarListagemSolicitacaoCadastramentoEditorasPO.retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaCNPJ();
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.aprovarReprovar();
    }
    
    @Quando("apresentar um popup com as opcoes de aprovar/reprovar e o campo justificativa$")
    public void quandoApresntarUmPopupComasOpcoesDeAprovarReprovarEOCampoJustificativa()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar um popup com as opcoes de aprovar/reprovar e o campo justificativa.");
    	
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfPageModalSolicitacao());
    }
    
    @E("clico na opcao aprovar/reprovar do popup de solicitacoes de cadastramento de editoras$")
    public void eClicoNaOpcaoAprovarReprovarDoPopupDeSoliciacoesDeCadastramentoDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico na opcao aprovar/reprovar do popup de solicitacoes de cadastramento de editoras");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("opcao")) 
    	{
			case "aprovar":
				pesquisarListagemSolicitacaoCadastramentoEditorasPO.aprovar();
				break;
	
			case "reprovar":
				pesquisarListagemSolicitacaoCadastramentoEditorasPO.reprovar();	
				break;
		}
    }
    
    @E("clico no botao confirmar do popup de solicitacoes de cadastramento de editoras$")
    public void eClicoNoBotaoConfirmarrDoPopupDeSolicitacoesDeCadastramentoDeEditoras()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao gravar do popup de solicitacoes de cadastramento de editoras.");
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.confirmarSolicitacao();
    }
    
    @Entao("devera voltar para a tela de listagem com a editora no filtro de aprovado/reprovado$")
    public void entaoDeveraVoltarParaATelaDeListagemComAEditoraNoFiltroDeAprovadoResprovado(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera voltar para a tela de listagem com a editora no filtro de aprovado/reprovado.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	switch (data.get(0).get("opcao")) 
    	{
			case "Aprovado":
				pesquisarListagemSolicitacaoCadastramentoEditorasPO.filtros(data.get(0).get("opcao"));
				break;
	
			case "Reprovados":
				pesquisarListagemSolicitacaoCadastramentoEditorasPO.filtros(data.get(0).get("opcao"));
				break;
		}
    	
    	assertEquals(cnpjEditora, pesquisarListagemSolicitacaoCadastramentoEditorasPO.retornoDosTextosDasColunasDaListagemDeSolicitacaoColunaCNPJ());
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
    
    @E("informarei o campo justificativa do popup de solicitacoes de cadastramento de editoras$")
    public void eInformareiOCampoJustificativaDoPopupDeSoliciacoesDeCadastramentoDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei o campo justificativa do popup de solicitacoes de cadastramento de editoras.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.justificativa(data.get(0).get("justificativa"));
    }
    
    @E("clico no botao historico de uma das solicitacoes de cadastramento de editora$")
    public void eClicoNoBotaoHistoricoDeUmaDasSoliciacoesDeCadastramentoDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao historico de uma das solicitacoes de cadastramento de editora.");
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.historico();
    }
    
    @Quando("apresentar a grid com o historico da solicitacao do cadastramento da editora$")
    public void quandoApresentarAGridComOHistoricoDaSolicitacaoDoCadastramentoDaEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a grid com o historico da solicitacao do cadastramento da editora.");
    	
    	pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfGridListagemHistoricoSolicitacao();
    }
    
    @Entao("devera apresentar os devidos campos na grid de historico$")
    public void entaoDeveraApresentarOsDevidosCamposNaGridDeHistorico(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar os devidos campos na grid de historico.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	for(int a = 0; a <= 6; a++)
    	{
    		switch (a) 
    		{
				case 0:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_1")));
					break;
	
				case 1:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_2")));
					break;
					
				case 2:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_3")));
					break;
					
				case 3:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_4")));
					break;
					
				case 4:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_5")));
					break;
					
				case 5:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_6")));
					break;
					
				case 6:
					assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfColunsOfListOfHistoricoDaSolicitacao(data.get(0).get("coluna_7")));
					break;
			}
    		
    		hooks.status = Status.PASS;
        	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    	}
    }
    
    @E("clico no botao justificativa da solicitacao do cadastramento da editora$")
	public void eClicoNoBotaoJustificativaDaSolicitacaoDoCadastramentoDaEditora()
	{
		geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao justificativa da solicitacao do cadastramento da editora.");
		
		pesquisarListagemSolicitacaoCadastramentoEditorasPO.justificativa();
	}
    
    @Entao("devera apresentar a modal com a justificativa daquele registro de reprovacao$")
    public void entaoDeveraApresentarAModalComAJustificativaDaqueleRegistroDeReprovacao()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar a modal com a justificativa daquele registro de reprovacao.");
    	
    	assertTrue(pesquisarListagemSolicitacaoCadastramentoEditorasPO.validationOfExibitionOfPageModalJustificativaDaSolciitacao());
    	
    	assertNotEquals("", pesquisarListagemSolicitacaoCadastramentoEditorasPO.retornoDosTextosDaJustificativaDaResprovacaoDaSolicitacao());
	
    	if(hooks.cenario.equals("Validar o modal de justificativa do historico de reprovacao")) 
    		hooks.ultimoScenario = true;
    	
    	hooks.status = Status.PASS;
    	geral.extentReport.SalvarLogScreenShotSucesso("Validacao realisada com sucesso.");
    }
}
