package steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import pageObject.editoraPO;

public class pesquisarVincularEditoraNaoVinculadaSteps
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    editoraPO editoraPO = new editoraPO();
    
    public pesquisarVincularEditoraNaoVinculadaSteps()
    {
         this.extentTest = geral.extentReport.test;
    }
    
    @E("clico no botao vincular editora$")
    public void eClicoNoBotaoVincularEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao vincular editora.");
    	
    	pageObject.editoraPO.vincularEditora();
    }
    
    @Quando("apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa$")
    public void quandoApresentarUmPopupComUmaListaDeEditorasNaoVinculadasAoObjetoEUmCampoEBotaoParaPesquisa()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar um popup com uma lista de editoras nao vinculadas ao objeto e um campo e botao para pesquisa.");
    	
    	assertTrue(pageObject.editoraPO.validacaoDeExibicaoDoPopupDeEditorasNaoVinculadas());
    }
    
    @Entao("devera apresentar uma lista de editoras nao vinculadas exibindo as colunas$")
    public void entaoDeveraApresentarUmalistaDeEditorasNaoVinculadasExibindoAsColunas(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma lista de editoras nao vinculadas exibindo as colunas.");
    	
    	assertTrue(editoraPO.validacaoDeExibicaoDaColunaDeCNPJNoPopupDeEditorasNaoVinculadas());
    	
    	assertTrue(editoraPO.validacaoDeExibicaoDaColunaDeEmpresaNoPopupDeEditorasNaoVinculadas());
    }
    
    @E("irei informar no campo pesquisar do popup de vincular editora$")
    public void eIreiInformarNocampoPesquisarDoPopupDeVincularEditora(DataTable dataTable)
    {
    	extentReport.SalvarLogScreenShotSucesso("E irei informar no campo pesquisar do popup de vincular editora.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	editoraPO.pesquisaEditorasNaoVinculadas(data.get(0).get("pesquisar"));
    }
    
    @E("clico no botao pesquisar do popup de vincular editora$")
    public void eClicoNoBotaoPesquisaDoPopupDeVincularEditora()
    {
    	extentReport.SalvarLogScreenShotSucesso("E clico no botao pesquisar do popup de vincular editora.");
    	
    	editoraPO.pesquisarEditorasNaoVinculadas();
    }
    
    @Entao("devera retornar o registro especifico pesquisado da editora nao vinculada$")
    public void entaoDeveraRetornarORegistroEspecificoPesquisadoDaEditoraNaoVinculada(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera retornar o registro especifico pesquisado da editora nao vinculada.");
    	List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	assertEquals(data.get(0).get("pesquisar"), pageObject.editoraPO.ReturnarRazaoSocialDaEditoraPesquisadaNoPopupDeEditorasNaoVinculadas());
    }
    
    @E("seleciono uma editora para fazer a vinculaçao$")
    public void eSelecionoUmaEditoraParaFazerAVinculacao()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao vincular editora.");
    	
    	pageObject.editoraPO.selecionarEditoraParaVinculadacao(1);
    }
    
    @E("clico no botao vincular do popup de vincular editora$")
    public void eClicoNoBotaoVinvularDoPopupDeVincularEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao vincular editora.");
    	
    	pageObject.editoraPO.vincularEditoraAoObjeto();
    }
}
