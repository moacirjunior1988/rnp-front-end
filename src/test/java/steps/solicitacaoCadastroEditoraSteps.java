package steps;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;

import cucumber.api.DataTable;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import geral.extentReport;
import geral.hooks;
import geral.testConfiguration;
import geral.utils;
import junit.framework.Assert;
import pageObject.loginPO;
import pageObject.solicitacaoCadastroEditoraPO;

public class solicitacaoCadastroEditoraSteps 
{
	protected ExtentTest extentTest;
	testConfiguration configuration = new testConfiguration();
    utils utils = new utils();
    extentReport extentReport = new extentReport();
    
    public solicitacaoCadastroEditoraSteps()
    {
		 this.extentTest = geral.extentReport.test;
    }
    
    @E("clico no link de solicitar acesso$")
    public void ClicoNoLinkDeSolicitarAcesso()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no link de solicitar acesso.");
    	
    	solicitacaoCadastroEditoraPO.Solicitacao();
    }
    
    @Quando("apresentar a tela solicitacao de cadastro de editora$")
    public void quandoApresentarATelaSolicitacaoDeCadastroDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Quando apresentar a tela solicitacao de cadastro de editora.");
    	
    	assertTrue(solicitacaoCadastroEditoraPO.validationOfExibitionOfPageSolicitacao());
    }
    
    @E("informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora$")
    public void eInformareiCorretamenteTodosOsCamposObrigatoriosDaSolicitacaoDeCadastroDeEditora(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E informarei corretamente todos os campos obrigatorios da solicitacao de cadastro de editora.");
    	
		List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
    	
    	String nomeResponsavel = "Nome Responsavel " + utils.geradorNumeroRandomico(4),
    			razaoSocial = "Editora Teste "+ utils.geradorNumeroRandomico(4);
    	
    	if(data.get(0).get("cnpj").equals("inserir")) solicitacaoCadastroEditoraPO.cnpj(utils.createCNPJ());
    	else solicitacaoCadastroEditoraPO.cnpj(data.get(0).get("cnpj"));
    	
    	if(data.get(0).get("razaoSocial").equals("inserir")) solicitacaoCadastroEditoraPO.razaoSocial(razaoSocial);
    	else solicitacaoCadastroEditoraPO.razaoSocial(data.get(0).get("razaoSocial"));
    	
    	if(data.get(0).get("nomeFantasia").equals("inserir")) solicitacaoCadastroEditoraPO.nomeFantasia(razaoSocial);
    	else solicitacaoCadastroEditoraPO.nomeFantasia(data.get(0).get("nomeFantasia"));
    	
    	if(data.get(0).get("cpfResponsavel").equals("inserir")) solicitacaoCadastroEditoraPO.cpfResponsavel(utils.GeradorCpf());
    	else solicitacaoCadastroEditoraPO.cpfResponsavel(data.get(0).get("cpfResponsavel"));
    	
    	if(data.get(0).get("nomeResponsavel").equals("inserir")) solicitacaoCadastroEditoraPO.nomeResponsavel(nomeResponsavel);
    	else solicitacaoCadastroEditoraPO.nomeResponsavel(data.get(0).get("nomeResponsavel"));
    	
    	if(data.get(0).get("telefoneResponsavel").equals("inserir")) solicitacaoCadastroEditoraPO.telefoneResponsavel(utils.createPhoneNumber());
    	else solicitacaoCadastroEditoraPO.telefoneResponsavel(data.get(0).get("telefoneResponsavel"));

    	if(data.get(0).get("emailResponsavel").equals("inserir")) solicitacaoCadastroEditoraPO.emailResponsavel(utils.createEmailMailinator(nomeResponsavel));
    	else solicitacaoCadastroEditoraPO.emailResponsavel(data.get(0).get("emailResponsavel"));
    	
    	if(data.get(0).get("vincularEditalObjeto").equals("inserir")) solicitacaoCadastroEditoraPO.vincularAoEdital("teste");
    	else solicitacaoCadastroEditoraPO.vincularAoEdital(data.get(0).get("vincularEditalObjeto"));
    }
    
    @E("clico no botao solicitar da tela de solicitacao de cadastro de editora$")
    public void eClicoNoBotaoSolicitarDaTelaDeSolicitacaoDeCadastroDeEditora()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E clico no botao solicitar da tela de solicitacao de cadastro de editora.");
    	
    	solicitacaoCadastroEditoraPO.solicitar();
    }
    
    @E("sou redirecionado para a tela de login do sistema$")
    public void eSouRedirecionadoParaATelaDeLoginDoSistema()
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("E sou redirecionado para a tela de login do sistema.");
    	
    	assertTrue(loginPO.validationOfExibitionOfPage());
    }
    
    @Entao("devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras$")
    public void entaoDeveraApresentarUmaMensagemDeValidacaoDaSolicitacaoDeCadastramentoDeEditoras(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma mensagem de validacao da solicitacao de cadastramento de editoras.");
    
    	switch (hooks.cenario) 
    	{
			case "Validar solicitacao de cadastro de editora com dados validos":
				
				break;
	
			case "Validar solicitacao de cadastro de editora nao informando nome fantasia":
				
				break;
					
			case "Validar solicitacao de cadastro de editora com cnpj inválido":
				
				break;
				
			case "Validar solicitacao de cadastro de editora com cpf do responsavel invalido":
				
				break;
				
			case "Validar regra de duplicidade ao solicitar um cadastro de uma nova editora":
			
				break;
				
			case "Validar solicitacao de cadastro de editora com responsável ja cadastrado no sistema":
				
				break;
		}
    }
    
    @Entao("devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras$")
    public void entaoDevereApresentarUmaMensagemDeValidacaoDosCamposObrigatoriosDoSolicitacaoDeCadastramentoDeEditoras(DataTable dataTable)
    {
    	geral.extentReport.SalvarLogScreenShotSucesso("Entao devera apresentar uma mensagem de validacao dos campos obrigatorios do solicitacao de cadastramento de editoras.");
    	
    	switch (hooks.cenario) 
    	{
	    	case "Validar campo obrigatorio cnpj da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio razao social da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio cpf do responsavel da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio nome do responsavel da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio telefone do responsavel da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio email do responsavel da solicitacao de cadastro de editora":
				
				break;
				
			case "Validar campo obrigatorio vincular edital ao objeto da solicitacao de cadastro de editora":
				
				break;
		}
    }
}
