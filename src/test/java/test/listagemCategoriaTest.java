package test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/PNLD_Digital_ListagemCategorias.feature",
        tags = {"@finalizado"},
        glue = {""},
        monochrome = true,
        dryRun = false)

public class listagemCategoriaTest {

}
